# NoSQLite

NoSQLite is a "nosql" style database written in Python that leverages
SQLite3 as its storage backend. This library is inspired by MongoDB and follows
similar patterns. The library allows for storing arbitrary JSON documents into
collections (similar to tables or groups), retrieving these documents directly
by id (UUID4), retrieving all documents from a given collection, and querying
documents based on simple query expressions. The library also allows for
indexing document fields and querying these indexes for efficient retrieval. 


## Dependencies

* Python 2.7 or 3.2+
* (optional) bson (if you want to store bson instead of json)
* (optional) nose (for running tests)
* (optional) bottlepy (for REST api)
* (optional) mimeparse (for REST api)
* (optional) sqlalchemy and psycopg2 (for Postgres backend)


## Installation

Just the library:

    curl -o nosqlite.py https://bitbucket.org/widefido/nosqlite/raw/tip/src/nosqlite.py 

Using pip:

    pip -e git+https://bitbucket.org/widefido/nosqlite.git#egg=nosqlite


The source repository (with tests):

    hg clone http://bitbucket.org/widefido/nosqlite


## Example

    >>> import nosqlite
    >>> store = nosqlite.Storage(":memory:")
    >>> store.insert("posts", {
    ...     "title":"Hello World",
    ...     "author":"Jordan Sherer",
    ...     "body":"Lorem ipsum..."})
    >>> p = store.find("posts", {"author":"Jordan Sherer"}, use_natural=True)
    >>> print(p[0]["title"])
    Hello World
    >>> print(p[0]["body"])
    Lorem ipsum...


## TODO

1. Postgres Database Driver (in progress. see: nosqlgre.py & test_postgres.py)
2. MongoDB-esque Atomic Operations (in progress. see nosqlite.py, update.py, & test_update.py)
    - set, unset
    - inc, dec
    - push, pushAll
    - pop, popAll
3. Partial Index Updates
    - Currently this is drop all indexes and reindex the document when the document changes.
4. Covered Indexes (return data from the indexes only, not the entity table)
5. A ResultSet explain for better understanding the SQL that is being generated and executed.
