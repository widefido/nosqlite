create table entry (
    id integer primary key, 
    parent_id integer,
    value text,
    FOREIGN KEY (parent_id) REFERENCES entry(id)
);

create table entry_closure (
    parent_id integer,
    child_id integer,
    depth integer,
    FOREIGN KEY (parent_id) REFERENCES entry(id),
    FOREIGN KEY (child_id) REFERENCES entry(id)
);

create trigger if not exists insert_entry after insert on entry
for each row begin
    -- insert the self node.
    insert into entry_closure (parent_id, child_id, depth) values (new.id, new.id, 0);
    
    -- insert the parent links
    insert into entry_closure (parent_id, child_id, depth)
        select p.parent_id, c.child_id, p.depth + c.depth + 1
        from entry_closure p, entry_closure c
        where p.child_id = new.parent_id and c.parent_id = new.id;
end;

create trigger if not exists delete_entry before delete on entry
for each row begin
    -- delete node from tree along with the subtree
    delete from entry_closure where rowid in (select distinct link.rowid from entry_closure link
        left outer join entry_closure p
        left outer join entry_closure c
        left outer join entry_closure to_delete
    where
        p.parent_id = link.parent_id and c.child_id = link.child_id
        and p.child_id = to_delete.parent_id and c.parent_id = to_delete.child_id
        and (to_delete.parent_id = old.id or to_delete.child_id = old.id)
        and to_delete.depth < 2);
end;

create trigger if not exists update_entry after update of parent_id on entry
for each row begin
    -- delete the ancestor links from the starting node id
    delete from entry_closure where
        entry_closure.child_id in (select child_id from entry_closure where parent_id = old.id)
        and entry_closure.parent_id not in (select child_id from entry_closure where parent_id = old.id);

    -- insert new ancestor links
    insert into entry_closure (parent_id, child_id, depth)
    select p.parent_id, c.child_id, p.depth + c.depth + 1
        from entry_closure p join entry_closure c
        where c.parent_id = old.id and p.child_id = new.parent_id;
end;

insert into entry (value) VALUES ('A');
insert into entry (parent_id, value) VALUES (1, 'B');
insert into entry (parent_id, value) VALUES (1, 'C');
insert into entry (parent_id, value) VALUES (2, 'B-1');
insert into entry (parent_id, value) VALUES (1, 'D');
insert into entry (parent_id, value) VALUES (2, 'B-2');
insert into entry (parent_id, value) VALUES (5, 'D-1');
insert into entry (parent_id, value) VALUES (7, 'D-1-1');

select "";
select "--entry--";
select * from entry;

select "";
select "--entry-closure--";
select * from entry_closure;

select "";
select "--roots--";
select entry.value from entry where entry.parent_id is NULL;

select "";
select "--entry-tree-descendents (of the root: id=1)--";
select entry_closure.depth, entry.id, entry.parent_id, entry.value from entry
    inner join entry_closure on entry.id = entry_closure.child_id
    where entry_closure.parent_id = 1;

select "";
select "--entry-tree-ancestors (of the leaf: value=D-1-1)--";
select entry_closure.depth, entry.id, entry.parent_id, entry.value from entry
    inner join entry_closure on entry.id = entry_closure.parent_id
    where entry_closure.child_id = 8 order by entry_closure.depth desc;


select "";
select "--entry-remove-subtree (of node: value=D)--";
delete from entry where id=5;
select entry_closure.depth, entry.id, entry.parent_id, entry.value from entry
    inner join entry_closure on entry.id = entry_closure.child_id
    where entry_closure.parent_id = 1;


--        A                      A
--       /|\                    / \
--      B C D                  C   D
--     /|\    \       =>       |    \    
--   B1 B2 B3  D1              B     D1
--              \             /|\     \
--              D11         B1 B2 B3  D11
select "";
select "--entry-move-subtree (node: B(2) from parent A(1) to C(3)--";

insert into entry (parent_id, value) values (1, 'D');
insert into entry (parent_id, value) values (9, 'D1');
insert into entry (parent_id, value) values (10, 'D11');

update entry set parent_id = 3 where id = 2;

select entry_closure.depth, entry.id, entry.parent_id, entry.value from entry
    inner join entry_closure on entry.id = entry_closure.child_id
    where entry_closure.parent_id = 1;


select "";
select "--entry-add-to-subtree-- (make X(12) the parent of the B(2) subtree)";

insert into entry (parent_id, value) values (null, "X");
update entry set parent_id = 12 where id = 2;

select "";
select "--roots--";
select entry.value from entry where entry.parent_id is NULL;

select "";
select "--A(1) tree--";
select entry_closure.depth, entry.id, entry.parent_id, entry.value from entry
    inner join entry_closure on entry.id = entry_closure.child_id
    where entry_closure.parent_id = 1;

select group_concat(n.value, ' -> ') as path from entry_closure d
    join entry_closure a on (a.child_id = d.child_id)
    join entry n on (n.id = a.parent_id)
    where d.parent_id = 1 and d.child_id != d.parent_id
    group by d.child_id;

select "";
select "--X(12) tree--";
select entry_closure.depth, entry.id, entry.parent_id, entry.value from entry
    inner join entry_closure on entry.id = entry_closure.child_id
    where entry_closure.parent_id = 12;


select "";
select "-- update all tree --";

delete from entry_closure;
--select count(*) from entry_closure;
insert into entry_closure select id, id, 0 from entry;
--select count(*) from entry_closure;
update entry set parent_id = parent_id;
--select count(*) from entry_closure;

select "--A(1) tree--";
select entry_closure.depth, entry.id, entry.parent_id, entry.value from entry
    inner join entry_closure on entry.id = entry_closure.child_id
    where entry_closure.parent_id = 1;

select "--X(12) tree--";
select entry_closure.depth, entry.id, entry.parent_id, entry.value from entry
    inner join entry_closure on entry.id = entry_closure.child_id
    where entry_closure.parent_id = 12;
