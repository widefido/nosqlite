db = Database()

search = "%Ashcroft%"

q = db.query("posts").where("title LIKE ? AND created <= NOW", [search]).orderby("created DESC")

for document in q.fetch():
    print(document)
