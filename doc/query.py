import pprint

class Error(Exception): pass
class ParseError(Error): pass

def itemize(query):
    """
    Itemize a query into a tuple of its components

    """
    if not isinstance(query, dict) and not isinstance(query, list):
        return query
    
    if not query:
        return None

    if isinstance(query, dict):
        items = query.items()
    elif isinstance(query, list):
        items = enumerate(query)
        #raise ParseError("lists not supported in query")
    else:
        items = iter(query)
    
    values = tuple((key, itemize(value)) for key, value in items)
    
    return values if len(values) != 1 else values[0]

def parse(query):
    print(itemize(query))

def main():
    parse({"title":"Yes", "author":"Jordan"}) 
    #parse({"title":{"$regex":"^a"}, "author":"Jordan"})
    #parse({"title":{"$neq":"Test", "$regex":["^a", "^b"]}})
    #title != "Test" and title REGEX '^a'
    #parse({"$or":{"title":{"$neq":"Test", "$or":{"$regex":["^a", "^b"]}}}})
    #title != "Test" or title REGEX '^a'
    #parse({"title":{"$neq":"", "$or":{"$neq":"Test", "$regex":"^a"}}})
    #(title, (($neq, ""), 
    #title != "" and (title != "Test" or title REGEX '^a')

    #parse({"title":"Test", "author":"Jordan"})
    # ("title = ? and author = ?", ["Test", "Jordan"])
    # parse({"title":"Test", "author":"Jordan", "tags":["a", "b"]})
    # title = ? and author = ? and (tags = ? or tags = ?), ["Test", "Jordan", "a", "b"]


if __name__ == "__main__":
    main()
