import pprint


class Error(Exception): pass
class ParseError(Error): pass


def itemize_(query):
    """
    Itemize a query into a tuple of its components

    """
    if not isinstance(query, dict) and not isinstance(query, list):
        return query
    
    if not query:
        return None

    if isinstance(query, dict):
        items = query.items()
    elif isinstance(query, list):
        items = enumerate(query)
        #raise ParseError("lists not supported in query")
    else:
        items = iter(query)
    
    values = tuple((key, itemize(value)) for key, value in items)
    
    return values if len(values) != 1 else values[0]


class Node(object):
    __slots__ = ("parent", "children", "value")
    def __init__(self, parent=None, value=None):
        self.children = []
        self.parent = parent
        if parent:
            self.parent.children.append(self)
        self.value = value

    def __str__(self):
        if self.value:
            if not self.children:
                return self.value
            if len(self.children) == 1:
                return "({0}, {1})".format(self.value, self.children[0])
            return "({0}, {1})".format(self.value, self.children)
        else:
            if len(self.children) == 1:
                return "{0}".format(self.children[0])
            return "{0}".format(self.children)

    def __repr__(self):
        return self.__str__()


def itemize__(parent, query):
    if not isinstance(query, dict) and not isinstance(query, list):
        return Node(parent, value=query)
    
    if not query:
        return Node(parent, value=query)

    if isinstance(query, dict):
        items = query.items()
        for key, value in items:
            n = Node(parent, value=key)
            itemize(n, value)
    elif isinstance(query, list):
        items = enumerate(query)
        for key, value in items:
            n = Node(parent)
            itemize(n, value)
        #raise ParseError("lists not supported in query")


def parse_(query):
    """
    1. itemize the query
    2. reorganize the query depending on infix/prefix operators
    3. convert the query items into a sql clause
    """
    root = Node()
    itemize(root, {"$and":query})
    print(root)


def etype(e):
    ef, ev = e
    return type(ev)




def parseMatchExpressionElement(e, andMatchers=None, orMatchers=None):
    if parseClause(e, andMatchers=andMatchers, orMatchers=orMatchers):
        return

    #parseWhere
    #parseRegex

    ef, ev = e
    if type(ev) is dict:
        for fe in ev.items():
            pass





class And(object):
    __slots__ = ("items",)
    def __init__(self, items):
        if not isinstance(items, dict) and not isinstance(items, list):
            raise ParseError("Item must be a list or dict")
        if not items:
            items = []
        self.items = items


class Or(object):
    __slots__ = ("items",)
    def __init__(self, items):
        if not isinstance(items, dict) and not isinstance(items, list):
            raise ParseError("Item must be a list or dict")
        if not items:
            items = []
        self.items = items



def parseClause(parent, key):
    pass

def parseExpressions(parent, key):
    pass

def parseMatchExpressionElement(parent, key):
    pass

def parse(query):
    """
    1. itemize the query
    2. reorganize the query depending on infix/prefix operators
    3. convert the query items into a sql clause
    """
    query = {"$and":query}

    
    print(query)
    

def main():
    parse({"title":{"$regex":"^a"}, "author":"Jordan"})
    #parse({"title":{"$neq":"Test", "$regex":["^a", "^b"]}})
    #title != "Test" and title REGEX '^a'
    #parse({"$or":{"title":{"$neq":"Test", "$or":{"$regex":["^a", "^b"]}}}})
    #title != "Test" or title REGEX '^a'
    #parse({"title":{"$neq":"", "$or":{"$neq":"Test", "$regex":"^a"}}})
    #(title, (($neq, ""), 
    #title != "" and (title != "Test" or title REGEX '^a')

    #parse({"title":"Test", "author":"Jordan"})
    # ("title = ? and author = ?", ["Test", "Jordan"])
    # parse({"title":"Test", "author":"Jordan", "tags":["a", "b"]})
    # title = ? and author = ? and (tags = ? or tags = ?), ["Test", "Jordan", "a", "b"]


if __name__ == "__main__":
    main()
