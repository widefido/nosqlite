#### DB

# GET     /                             => get_dbs(...)
curl -X GET -H 'Accept: application/json' http://localhost:8087/

# POST    /                             => create_db(...)
#         ?db=<db>
curl -X POST -H 'Accept: application/json' http://localhost:8087/?db=db

# DELETE  /<db>                         => delete_db(...)
curl -X DELETE -H 'Accept: application/json' http://localhost:8087/db


#### COLLECTIONS

# GET     /<db>                         => <db>.collections(...)
curl -X GET -H 'Accept: application/json' http://localhost:8087/db

# POST    /<db>                         => <db>.collection_create(...)
#         ?collection=<collection>
curl -X POST -H 'Accept: application/json' http://localhost:8087/db?collection=docs

# DELETE  /<db>/<collection>            => <db>.collection_drop(...)
curl -X DELETE -H 'Accept: application/json' http://localhost:8087/db/docs

 
#### DOCUMENTS

# GET     /<db>/<collection>            => <db>.find(...)
#         ?q=<query>
#         &fields=<fields>
#         &sort=<sort>
#         &skip=<skip>
#         &limit=<limit>
#         &use_natural=<use_natural>
curl -X GET -H 'Accept: application/json' http://localhost:8087/db/docs
?q=<query>
&fields=<fields:list>
&sort=<sort:string>
&skip=<skip:int>
&limit=<limit:int>
&use_natural=<use_natural:bool>

# POST    /<db>/<collection>            => <db>.insert(...)
#         ?omit_payload=<omit_payload>
curl -X POST -H 'Accept: application/json' -H 'Content-Type: application/json' -d '
{"title":"Test Document", "author":"Your Name Here"}' http://localhost:8087/db/docs
?omit_payload=<omit_payload:bool>

# GET     /<db>/<collection>/<id>       => <db>.retrieve(...)
curl -X GET -H 'Accept: application/json' http://localhost:8087/db/docs/123

# PUT     /<db>/<collection>/<id>       => <db>.update(...)
#         ?upsert=<upsert>
#         &multi=<multi>
#         &sort=<sort>
#         &skip=<skip>
#         &limit=<limit>
#         &omit_payload=<omit_payload>
curl -X PUT -H 'Accept: application/json' -H 'Content-Type: application/json' -d '
{"test":123}' http://localhost:8087/db/docs/123

# PATCH   /<db>/<collection>/<id>       => <db>.patch(...)
#         ?multi=<multi>
#         &sort=<sort>
#         &skip=<skip>
#         &limit=<limit>
#         &omit_payload=<omit_payload>
curl -X PATCH -H 'Accept: application/json' -H 'Content-Type: application/json' -d '
{"test":123}' http://localhost:8087/db/docs/123

# DELETE  /<db>/<collection>/<id>       => <db>.remove(...)
curl -X DELETE -H 'Accept: application/json' http://localhost:8087/db/docs/123


#### INDEXES
#
# GET     /<db>/<collection>/indexes    => <db>.collection_indexes(...)
curl -X GET -H 'Accept: application/json' http://localhost:8087/db/docs/indexes

# DELETE  /<db>/<collection>/indexes    => <db>.collection_drop_indexes(...)
curl -X DELETE -H 'Accept: application/json' http://localhost:8087/db/docs/indexes
 
# PUT     /<db>/<collection>/indexes    => <db>.collection_reindex_indexes(...)
curl -X PUT -H 'Accept: application/json' http://localhost:8087/db/docs/indexes

# POST    /<db>/<collection>/indexes    => <db>.index_ensure(...)
#         ?fields=<fields> (csv)
#         &unique=<unique>
#         &sparse=<sparse>,
#         &reindex_all=<reindex_all>
#         &key=<key>
curl -X POST -H 'Accept: application/json' http://localhost:8087/db/docs/indexes
?fields=<fields:list>
&unique=<unique:bool>
&sparse=<sparse:bool>,
&reindex_all=<reindex_all:bool>
&key=<key:string>

# GET     /<db>/indexes => <db>.indexes(...)
curl -X GET -H 'Accept: application/json' http://localhost:8087/db/indexes

# GET     /<db>/indexes/<index> => <db>.index_retrieve(...)
curl -X GET -H 'Accept: application/json' http://localhost:8087/db/indexes/index:docs:title

# DELETE  /<db>/indexes/<index> => <db>.index_drop(...)
curl -X DELETE -H 'Accept: application/json' http://localhost:8087/db/indexes/index:docs:title
 
#### FIN
