"""
MongoDB Style Atomic Update Operations
see: http://docs.mongodb.org/manual/reference/operators/#_S_where

"""
from nosqlite import META_KEYS

def _set(document, field, value):
    """
    This operator will add the specified field or fields if they do not exist
    in this document or replace the existing value of the specified field(s)
    if they already exist.

    {"$set":{"title":"Hello World", "author":"Jordan"}}

    """
    document[field] = value
    return document


def _unset(document, field, value):
    """
    This operator will remove the specified field or fields from this document
    if they exist. Otherwise, no action is performed.

    """
    if field in document:
        del document[field]
    return document


def _inc(document, field, amount):
    """
    This operator will increment a field or fields numeric value by the
    specified amount if the field is present in the document. Otherwise,
    this operation will set the field to the amount value.

    """
    if field in document:
        if hasattr(document[field], "__add__"):
            document[field] += amount
    else:
        document[field] = amount
    return document


def _rename(document, from_name, to_name):
    """
    This operator will rename a field or fields from one name to another if the
    from_name field exists in the document. Otherwise, no action is performed.

    """
    if from_name in document:
        document[to_name] = document[from_name]
        del document[from_name]
    return document


def _push(document, field, value):
    """
    This operator will push a value into an array specified by field. If field
    does not exist in the document, a new array is created. If field exists in 
    the document, but is not an array, no operation is performed.

    """
    if field in document:
        if isinstance(document[field], list):
            document[field].append(value)
    else:
        document[field] = [value]
    return document


def _push_all(document, field, values):
    """
    Similar to push, this operator will extend an array specified by field with
    a list of values. If field does not exist in the document, a new array 
    containing values is created. If field the exists in the document,
    but is not an array, no operation is performed. If values is not an array, 
    it is added to an array to be extended, in which the operation would be 
    performed as a "push".

    """
    if not isinstance(values, list):
        return _push(document, field, values)

    if field in document:
        if isinstance(document[field], list):
            document[field].extend(values)
    else:
        document[field] = values
    return document


def _pop(document, field, value):
    """
    This operator will remove the first or last element of an array. If value 
    is a positive integer, this operator will remove the last element in the 
    array. Otherwise, this operator will remove the first element of an array.
    If field does not exist in the document, or is not an array, or is an empty 
    array, no operation is performed.

    """
    if field in document and isinstance(document[field], list) and document[field]:
        if value > 0:
            document[field].pop()
        else:
            del document[field][0]
    return document


def _pull(document, field, value):
    """
    This operator will remove every instance of value from the array specified 
    by field. If field does not exist in the document, or is not an array, or 
    is an empty array, no operation is performed.

    """
    if field in document and isinstance(document[field], list) and document[field]:
        document[field] = [v for v in document[field] if v != value]
    return document


def _pull_all(document, field, values):
    """
    This operator will remove every instance of all values from the array
    specified by field. If field does not exist in the document, or is not an
    array, or is an empty array, no operation is performed. If values is not 
    an array, a pull operation is performed.

    """
    if not isinstance(values, list):
        return _pull(document, field, values)

    if field in document and isinstance(document[field], list) and document[field]:
        document[field] = [v for v in document[field] if v not in values]
    return document


def _add_to_set(document, field, value):
    """
    This operator will push a value to an array if and only if the value does
    not already exist in the array. If field does not exist in the document a
    new array is created. If field does exist in the document and 
    is not an array, no operation is performed.

    """
    if field in document:
        if isinstance(document[field], list) and value not in document[field]:
            document[field].append(value)
    else:
        document[field] = [value]
    return document


def _add_all_to_set(document, field, values):
    """
    This operator will extend an array with values if and only if the values do
    not already exist in the array. If field does not exist in the document, a
    new array is created. If field does exist in the document and is not an
    array, no operation is performed. If values is not an array, an 
    add_to_set operation is performed.

    """
    if not isinstance(values, list):
        return _add_to_set(document, field, values)

    if field in document:
        if isinstance(document[field], list):
            for value in values:
                if value in document[field]:
                    continue
                document[field].append(value)
    else:
        document[field] = [value]
    return document


update_operations = {
    "$set":_set,
    "$unset":_unset,
    "$inc":_inc,
    "$rename":_rename,
    "$push":_push,
    "$pushAll":_push_all,
    "$pull":_pull,
    "$pullAll":_pull_all,
    "$pop":_pop,
    "$addToSet":_add_to_set,
    "$addAllToSet":_add_all_to_set
}


def update_document(document, query, pure=False):
    """
    Given a document, perform a series of update operations as specified by 
    the provided update query.

    """
    update_ops = set(update_operations.keys())
    query_ops = set(query.keys())
    
    non_operations = query_ops - update_ops
    operations = query_ops & update_ops

    if pure and non_operations and operations:
        raise Exception("update cannot mix atomic and non-atomic operations")

    if non_operations and not operations:
        for k in list(document.keys()):
            if k in META_KEYS:
                continue
            del document[k]
        document.update(query)
        return document

    if non_operations:
        for op in non_operations:
            value = query[op]
            document[op] = value

    if operations:
        for op in operations:
            op_func = update_operations[op]
            for key, value in query[op].items():
                op_func(document, key, value)

    return document
