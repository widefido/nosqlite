"""
NoSQLite Test: Tests for the NoSQLite library

Copyright (c) 2012, Jordan Sherer
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""

__version__   = '0.0'
__author__    = 'Jordan Sherer <jordan@widefido.com>'
__license__   = 'BSD2'


import collections

import nosqlite


def create_storage():
    return nosqlite.Storage()


def test_dot():
    o = {"a": {"b": {"c": 1, "d": 2} } }

    assert(nosqlite.Dot.dot(o, "a.b.c") == 1)
    assert(nosqlite.Dot.dot(o, "a.b.d") == 2)

    nosqlite.Dot.dot(o, "a.x", value=10, op="set")
    assert(nosqlite.Dot.dot(o, "a.x") == 10)

    nosqlite.Dot.dot(o, "a.j.c", value=123, op="set")
    assert(nosqlite.Dot.dot(o, "a.j.c") == 123)

    nosqlite.Dot.dot(o, "a.j.c", op="del")
    assert(nosqlite.Dot.dot(o, "a.j") == {})


def test_dot2():
    o = nosqlite.Dot({"a": {"b": {"c": 1, "d": 2} } })

    assert(o["a.b.c"] == 1)
    assert(o["a.b.d"] == 2)

    assert("a" in o)
    assert("a.b" in o)
    assert("a.b.c" in o)

    o["a.x"] = 10
    assert(o["a.x"] == 10)

    o["a.j.c"] = 123
    assert(o["a.j.c"] == 123)

    del o["a.j.c"]
    assert(o["a.j"] == {})


def _test_dot3():
    """
    IGNORE THIS TEST: the Dot object doesn't support this method longer.
    """
    o = nosqlite.Dot({"a": {"b": {"c": 1, "d": 2} } })

    assert(o.a.b.c == 1)
    assert(o.a.b.d == 2)

    o.a.x = 10
    assert(o.a.x == 10)

    o.a.j.c = 123
    assert(o.a.j.c == 123)

    del o.a.j.c
    assert(o.a.j == {})


def test_serialization():
    o = {"test":123, "internal":{"test":345}}

    assert(nosqlite.serialize(o, use_pickle=False, compress=False) ==
        '{"test": 123, "internal": {"test": 345}}')
    assert(nosqlite.unserialize(nosqlite.serialize(o, use_pickle=False,
        compress=False), use_pickle=False, decompress=False) == o)
    
    assert(nosqlite.serialize(o, use_pickle=False, compress=True) ==
        b'x\x9c\xabV*I-.Q\xb2R042\xd6QP\xca\xcc+I-'
        b'\xcaK\xcc\x01\nT\xc3d\x8cMLkk\x01\xf7\xdd\x0c&')
    assert(nosqlite.unserialize(nosqlite.serialize(o, use_pickle=False,
        compress=True), use_pickle=False, decompress=True) == o)

    assert(nosqlite.unserialize(nosqlite.serialize(o, use_bson=True),
        use_bson=True) == o)
    assert(nosqlite.unserialize(nosqlite.serialize(o, use_bson=True,
        compress=True), use_bson=True, decompress=True) == o)


def test_sqlite_in_memory():
    def on_new(db):
        assert(db.table_exists("entity") == False)

    db = nosqlite.Database(":memory:", on_new=on_new)
    
    db.execute("""CREATE TABLE IF NOT EXISTS collection (
        name STRING PRIMARY KEY,
        payload BLOB )""")
    
    db.execute("""CREATE TABLE IF NOT EXISTS entity (
        added_id INTEGER PRIMARY KEY,
        collection STRING,
        uid UID UNIQUE,
        created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
        updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
        payload BLOB,
        FOREIGN KEY (collection) REFERENCES collection (name) )""")

    db.execute("CREATE INDEX idx_entity_uid ON entity (uid)")
    db.execute("CREATE INDEX idx_entity_collection ON entity (collection)")
    
    assert(db.table_exists("collection"))
    assert(db.table_exists("entity"))

    info = db.table_info("entity")
    assert(len(info["column_names"]) == 6)


def test_sqlite_context():
    db = nosqlite.Database(":memory:")
    
    try:
        with db:
            db.execute("CREATE TABLE IF NOT EXISTS test ( id INTEGER "
                "PRIMARY KEY, name TEXT UNIQUE )")
            db.execute("INSERT INTO test (name) VALUES ('a')")
            with db:
                db.execute("INSERT INTO test (name) VALUES ('a')")
            db.execute("INSERT INTO test (name) VALUES ('b')")
            db.commit()
    except:
        pass
    
    assert(db.table_exists("test"))
    assert(len(db.execute("SELECT * FROM test").fetchall()) == 0)

    with db:
        db.execute("CREATE TABLE IF NOT EXISTS test ( id INTEGER "
            "PRIMARY KEY, name TEXT UNIQUE )")
        db.execute("INSERT INTO test (name) VALUES ('a')")
        db.commit()
        try:
            with db:
                db.execute("INSERT INTO test (name) VALUES ('a')")
        except:
            pass
        db.execute("INSERT INTO test (name) VALUES ('b')")
    
    assert(db.table_exists("test"))
    assert(len(db.execute("SELECT * FROM test").fetchall()) == 2)


def test_sqlite_payload_field():
    """test custom json field function activated"""
    db = nosqlite.Database(":memory:")

    with db:
        db.execute("CREATE TABLE IF NOT EXISTS test ( id INTEGER "
            "PRIMARY KEY, payload BLOB )")
        obj1 = { "id":1, "fieldName1":"1fieldValue1",
            "fieldName2":"1fieldValue2", "nested":{ "a": 1 } }
        obj2 = { "id":2, "fieldName1":"2fieldValue1",
            "fieldName2":"2fieldValue2", "nested":{ "a": 2 } }
        obj3 = { "id":3, "nested":{ "a": 3 } }
        
        db.execute("INSERT INTO test (id, payload) VALUES (1, ?)",
            (nosqlite.serialize(obj1),))
        db.execute("INSERT INTO test (id, payload) VALUES (2, ?)",
            (nosqlite.serialize(obj2),))
        db.execute("INSERT INTO test (id, payload) VALUES (3, ?)",
            (nosqlite.serialize(obj3),))
        db.commit()

        c = db.execute("SELECT id, payload_field(rowid, payload, "
            "'fieldName1', NULL) as fieldName1 FROM test")
        assert(len(c.fetchall()) == 3)

        c = db.execute("SELECT id, payload_field(rowid, payload, "
            "'fieldName1', NULL) as fieldName1 FROM test WHERE "
            "fieldName1 IS NOT NULL")
        assert(len(c.fetchall()) == 2)
        for row in c.fetchall():
            assert(row["fieldName1"] == ("%sfieldValue1" % row["id"]))

        c = db.execute("SELECT id, payload_field(rowid, payload, "
            "'nested.a') as a FROM test")
        assert(len(c.fetchall()) == 3)
        for row in c.fetchall():
            assert(row["a"] == row["id"]) 


def test_sqlite_regexp():
    """test custom regexp function activated"""
    db = nosqlite.Database(":memory:")

    with db:
        db.execute("CREATE TABLE IF NOT EXISTS test ( id INTEGER "
            "PRIMARY KEY, content TEXT )")
        
        db.execute("INSERT INTO test (id, content) VALUES (1, ?)", ("111",))
        db.execute("INSERT INTO test (id, content) VALUES (2, ?)", ("11",))
        db.execute("INSERT INTO test (id, content) VALUES (3, ?)", ("1",))
        db.commit()

        c = db.execute("SELECT id FROM test WHERE content REGEXP '\d{1,}'")
        assert(len(c.fetchall()) == 3)

        c = db.execute("SELECT id FROM test WHERE content REGEXP '\d{2,}'")
        assert(len(c.fetchall()) == 2)

        c = db.execute("SELECT id FROM test WHERE content REGEXP '\d{3,}'")
        assert(len(c.fetchall()) == 1)


def test_storage():
    """test insert, retrieve, delete"""
    store = create_storage()
    
    assert(store.database.table_exists("entity"))

    a = store.insert("posts", {"title":"Hello"})
    assert(store.exists(a["_uid"]))

    posts = store.retrieve_all("posts")
    assert(posts is not None)
    assert(len(posts) == 1)

    assert(store.delete_document(posts[0]))
    posts = store.retrieve_all("posts")
    assert(len(posts) == 0)


def test_storage_context():
    """ test storage context using with statement """
    with create_storage() as store:
        assert(store.database.table_exists("entity"))
    

def test_storage2():
    """test insert, retrieve, update"""
    store = create_storage()
    
    o = store.insert("posts", {"title":"Hello"})
    o1 = store.retrieve(o["_uid"])
    assert(o["title"] == "Hello")
    assert(o["title"] == o1["title"])
    assert(o["_uid"] == o1["_uid"])

    o["title"] = "Holla"
    store.save_document(o)
    
    o1 = store.retrieve(o["_uid"])
    assert(o["title"] == "Holla")
    assert(o["title"] == o1["title"])
    assert(o["_uid"] == o1["_uid"])


def test_storage_index():
    """test indexes"""
    store = create_storage()
    
    b = store.index_ensure("posts", fields="meta.count", sparse=True)
    a = store.index_ensure("posts", fields="title", unique=True)
    
    key = nosqlite._index_key("posts", fields="title", unique=True)
    
    assert(store.index_exists(key))

    index = store.index_retrieve(key)
    assert(index)
    assert(index.collection == "posts")
    assert(index.fields == ("title",))
    assert(index.unique == True)
    assert(index.sparse == False)

    indexes = store.indexes()
    assert(len(indexes) == 2)
    assert(a.to_dict() == indexes[a.key].to_dict())
    assert(b.to_dict() == indexes[b.key].to_dict())

    try:
        store.insert("posts", {"title":"hello"})
    except:
        pass

    assert(len(store.retrieve_all("posts")) == 1)
    
    store.insert("posts", {"title":"hello-world", "meta":{"count":1}})

    assert(len(store.retrieve_all("posts")) == 2)

    store.insert("posts", {"meta":{"count":2}})

    #assert(len(store.database.execute('select * from '
    #    '"uniqueindex:posts:title"').fetchall()) == 3)
    #assert(len(store.database.execute('select * from '
    #    '"sparseindex:posts:meta.count"').fetchall()) == 2)


def test_storage_omit_payload():
    """test insert, retrieve, update"""
    store = create_storage()
    
    o = store.insert("posts", {"title":"Hello"}, omit_payload=True)
    o1 = store.retrieve_all("posts")
    assert(o1)
    assert(len(o1) > 0)
    assert("_payload" in o1[0])
    assert(o1[0]["_payload"] == None)

    store.index_ensure("posts", fields=["title"])
    o = store.insert("posts", {"title":"World"}, omit_payload=True)
    
    #posts = store.find("posts", fields=["title"])
    posts = store.retrieve_all("posts")
    assert(len(posts) == 2)
    assert(posts[0].get("title") == None)
    assert(posts[1].get("title") == None)

    # TODO: be able to return field values from index when omit_payload is True
    # assert(posts[0]["title"] == "World" or posts[1]["title"] == "World") 


def test_storage_collections():
    store = create_storage()
    store.index_ensure("posts", fields="title", unique=True)
    a = store.insert("posts", {"title":"a"})
    b = store.insert("posts", {"title":"b"})
    c = store.insert("posts", {"title":"c"})
    d = store.insert("posts", {"title":"d"})
    e = store.insert("posts", {"title":"e"})

    d1 = store.insert("docs", {"author":"Jordan", "title":"a"})

    assert(len(store.find("posts")) == 5)
    assert(len(store.find("docs")) == 1)

    assert(len(store.find("posts", {"title":"a"})) == 1)
    assert(len(store.find("docs", {"title":"a"}, use_natural=False)) == 0)
    assert(len(store.find("docs", {"title":"a"})) == 1)
    assert(len(store.find("docs", {"_uid":d1["_uid"]})) == 1)

    assert(len(store.collections()) == 2)
    assert(store.collections() == ["posts", "docs"])


def test_storage_reindex():
    """test reindexing of a collection"""
    store = nosqlite.Storage(":memory:")
    
    store.insert("posts", {"author":"Jordan Sherer", "content":"7"})
    store.insert("posts", {"author":"Sherer Jordan", "content":"42"})

    store.index_ensure("posts", fields="author", unique=True)
    store.collection_reindex_indexes("posts")


def test_storage_index_drop():
    """test the dropping of an index"""
    store = create_storage()
    index = store.index_ensure("posts", fields="title", unique=True)
    assert(index)

    key = nosqlite._index_key("posts", fields="title", unique=True)
    assert(key == index.key)
    assert(store.index_exists(key))

    store.index_drop(key)
    assert(not store.index_exists(key))


def test_storage_index_drop_all_in_collection():
    """test the dropping all of an indexes of a collection"""
    store = create_storage()
    store.index_ensure("authors", fields="name", unique=True)    
    store.index_ensure("posts", fields="title", unique=True)
    store.index_ensure("posts", fields="author")

    key0 = nosqlite._index_key("authors", fields="name", unique=True)
    assert(store.index_exists(key0))

    key1 = nosqlite._index_key("posts", fields="title", unique=True)
    assert(store.index_exists(key1))

    key2 = nosqlite._index_key("posts", fields="author")
    assert(store.index_exists(key2))

    store.collection_drop_indexes("posts")
    assert(not store.index_exists(key1))
    assert(not store.index_exists(key2))
    assert(store.index_exists(key0))


def test_storage_index_drop_all():
    """test the dropping all of an indexes of a collection"""
    store = create_storage()
    store.index_ensure("authors", fields="name", unique=True)    
    store.index_ensure("posts", fields="title", unique=True)
    store.index_ensure("posts", fields="author")

    store.index_drop_all()
    assert(len(store.indexes()) == 0)


def test_storage_drop_collection():
    """test the dropping an entire collection"""
    store = create_storage()
    store.index_ensure("authors", fields="name", unique=True)    
    store.index_ensure("posts", fields="title", unique=True)
    store.index_ensure("posts", fields="author")

    store.insert("authors", {"name":"Jordan Sherer"})
    store.insert("posts", {"title":"Test", "author":"Jordan Sherer"})

    key0 = nosqlite._index_key("authors", fields="name", unique=True)
    assert(store.index_exists(key0))

    key1 = nosqlite._index_key("posts", fields="title", unique=True)
    assert(store.index_exists(key1))

    key2 = nosqlite._index_key("posts", fields="author")
    assert(store.index_exists(key2))

    store.collection_drop("posts")
    assert(not store.index_exists(key1))
    assert(not store.index_exists(key2))
    assert(store.index_exists(key0))

    assert(not store.retrieve_all("posts"))
    assert(len(store.retrieve_all("authors")) == 1)


def test_storage_delete():
    store = create_storage()
    store.index_ensure("posts", fields="title", unique=True)
    a = store.insert("posts", {"title":"a"})
    b = store.insert("posts", {"title":"b"})
    c = store.insert("posts", {"title":"c"})
    d = store.insert("posts", {"title":"d"})
    e = store.insert("posts", {"title":"e"})

    assert(len(store.retrieve_all("posts")) == 5)
    store.delete_document(a)
    assert(len(store.retrieve_all("posts")) == 4)
    store.delete("posts", {"title":"d"})
    assert(len(store.retrieve_all("posts")) == 3)
    store.delete("posts", None, limit=2)
    assert(len(store.retrieve_all("posts")) == 1)


def test_storage_find_nested_fields():
    store = create_storage();

    store.insert("posts", {"author":{"fname":"Jordan"}})

    p = store.find_one("posts", fields="author.fname", skip=0)
    assert(p)
    assert(p["author"])
    assert(p["author"]["fname"] == "Jordan")


def test_storage_unindexed_eq_find_with_reindex():
    store = create_storage()

    store.insert("posts", {"title":"a"})
    store.insert("posts", {"title":"a"})
    store.insert("posts", {"title":"b"})
    store.insert("posts", {"title":"c"})
    store.insert("posts", {"title":"d"})
    store.insert("posts", {"title":"e"})

    # use_natural = False (i.e., only use indexed fields)
    assert(len(store.find("posts", {"title":"a"}, use_natural=False)) == 0)
    assert(len(store.find("posts", {"title":"a"})) == 2)

    store.index_ensure("posts", fields="title", reindex_all=True)

    assert(len(store.find("posts", {"title":"a"})) == 2)


def test_storage_find_advanced_operators():
    store = create_storage()

    store.insert("posts", {"title":"a"})
    store.insert("posts", {"title":"b"})
    store.insert("posts", {"title":"c"})
    store.insert("posts", {"title":"d"})
    store.insert("posts", {"title":"e"})

    store.index_ensure("posts", fields="title", reindex_all=True)

    # use_natural = False (i.e., only use indexed fields)
    assert(len(store.find("posts", {})) == 5)

    assert(len(store.find("posts", {"title":{"$lte":"c"}})) == 3)
    assert(len(store.find("posts", {"title":{"$gte":"b"}})) == 4)
    assert(len(store.find("posts", {"title":{"$regex":"\d+"}})) == 0)

    store.insert("posts", {"title":"f1", "new":True})
    
    assert(len(store.find("posts", {"title":{"$regex":"\d+"}})) == 1)
    assert(store.find("posts",
            {"title":{"$regex":"\d+"}})[0]["new"] == True)

    assert(len(store.find("posts", {"title":{"$gt":"a", "$lt":"e"}})) == 3)

    assert(len(store.find("posts", {"new":True}, use_natural=False)) == 0)
    assert(len(store.find("posts", {"new":True})) == 1)
    assert(len(store.find("posts", {"new":{"$eq":True}}, use_natural=False)) == 0)
    assert(len(store.find("posts", {"new":{"$eq":True}})) == 1)

    store.index_ensure("posts", fields="tags")
    store.insert("posts", {"title":"x1", "tags":["a","b","c"]})
    store.insert("posts", {"title":"x2", "tags":["a","b"]})
    store.insert("posts", {"title":"x3", "tags":["a"]})

    r = store.find("posts", {"tags":"a"})
    assert(len(r) == 3)


def test_storage_find_skip_limit_sort():
    store = create_storage()

    store.index_ensure("posts", fields="title")
    store.insert("posts", {"title":"a"})
    store.insert("posts", {"title":"b"})
    store.insert("posts", {"title":"c"})
    store.insert("posts", {"title":"d"})
    store.insert("posts", {"title":"e"})
    store.insert("posts", {"title":"f"})
    store.insert("posts", {"title":"g", "new":True})

    p = store.find_one("posts", skip=1)
    assert(p["title"] == "b")

    assert(store.find_count("posts") == 7)
    assert(store.find_count("posts", limit=3) == 3)
    assert(store.find_count("posts", limit=10) == 7)

    assert(len(store.find("posts", limit=3)) == 3)
    assert(len(store.find("posts", limit=10)) == 7)

    d = store.find("posts", skip=2, limit=2)
    assert(len(d) == 2)
    assert(d[0]["title"] == "c")
    assert(d[1]["title"] == "d")
    
    d = store.find("posts", sort={"title":-1}, limit=1)
    assert(len(d) == 1)
    assert(d[0]["title"] == "g")
    
    try:
        # make sure this raises a QueryError
        # because we are sorting on a non-natural, non-indexed field
        d = store.find("posts", sort="""{"new":1, "title":-1}""", limit=2, use_natural=False)
        assert(False)
    except nosqlite.QueryError:
        assert(True)
    

    d = store.find("posts", sort="""{"new":-1, "title":-1}""", limit=3)
    assert(len(d) == 3)
    assert(d[0]["title"] == "g")
    assert(d[1]["title"] == "f")
    assert(d[2]["title"] == "e")

    sort = collections.OrderedDict([("new", -1), ("title", -1)])
    d = store.find("posts", sort=sort, limit=3, use_natural=True)
    assert(len(d) == 3)
    assert(d[0]["title"] == "g")
    assert(d[1]["title"] == "f")
    assert(d[2]["title"] == "e")

    d = store.find("posts", sort={"new":-1, "title":-1}, skip=1, limit=3, use_natural=True)
    assert(len(d) == 3)
    assert(d[0]["title"] == "f")
    assert(d[1]["title"] == "e")
    assert(d[2]["title"] == "d")


    d = store.find("posts", sort={"title":-1})
    assert(len(d) == 7)
    assert(d[0]["title"] == "g")


def test_sorting():
    store = create_storage()

    store.index_ensure("posts", fields="title")
    store.insert("posts", {"title":"a", "num":6})
    store.insert("posts", {"title":"b", "num":5})
    store.insert("posts", {"title":"c", "num":4})
    store.insert("posts", {"title":"d", "num":3})
    store.insert("posts", {"title":"e", "num":2})
    store.insert("posts", {"title":"f", "num":1})

    try:
        # this should throw because the sort dictionary is not ordered, 
        # meaning that the sort is actually num:1, title:1, num is sorted first.
        p = store.find("posts", sort={"title":1, "num":1}, use_natural=True)
        assert(p[0]["title"] == "a")
    except:
        assert(True)
    else:
        assert(False)

    # to solve, use an ordered dict or a json string (which will be loaded as
    # an ordered dict when parsed)
    p = store.find("posts", sort='{"title":1, "num":1}', use_natural=True)
    assert(p[0]["title"] == "a")

    p = store.find("posts", sort=collections.OrderedDict([("title", 1), ("num", 1)]), use_natural=True)
    assert(p[0]["title"] == "a")



def test_storage_find_where_groups():
    store = create_storage()

    store.index_ensure("posts", fields="title")
    store.insert("posts", {"title":"a"})
    store.insert("posts", {"title":"b"})
    store.insert("posts", {"title":"c"})
    store.insert("posts", {"title":"d"})
    store.insert("posts", {"title":"e"})
    store.insert("posts", {"title":"f"})
    store.insert("posts", {"title":"g", "new":True})
    
    clause = {"$or":[
                {"$or":[
                    {"title":"a"}, 
                    {"title":"b"}
                ]}, 
                {"title":"c"}]
            }
    assert(len(store.find("posts", clause)) == 3)

    clause = {"$or":[{"title":"a"}, {"new":True}]}
    assert(len(store.find("posts", clause, use_natural=False)) == 0)
    assert(len(store.find("posts", clause)) == 2)


def test_update_query():
    store = create_storage()

    store.index_ensure("posts", fields="title")
    a = store.insert("posts", {"title":"a"})
    b = store.insert("posts", {"title":"b"})
    c = store.insert("posts", {"title":"c"})

    # update based on a simple query
    a1 = store.update("posts", {"title":"a"}, {"title":"x", "new":True})
    assert(a1)
    assert("_uid" in a1)
    assert("_collection" in a1)
    assert("_created" in a1)
    assert("_updated" in a1)

    x = store.retrieve(a["_uid"])
    assert(x)
    assert(x["title"] == "x")
    assert(x["new"] == True)

    # update based on a complex query
    q = {"title":{"$gt":"a", "$lte":"c"}}
    store.update("posts", q, {"title":"x", "author":"Jordan"})
    x = store.retrieve(b["_uid"])
    c = store.retrieve(c["_uid"])
    assert(x)
    assert(x["title"] == "x")
    assert(x["author"] == "Jordan")
    assert(c)
    assert(c["title"] == "c")

    store.update("posts", q, {"title":"x", "author":"Jordan Sherer"})
    c = store.retrieve(c["_uid"])
    assert(c)
    assert(c["title"] == "x")
    assert(c["author"] == "Jordan Sherer")

    # update using a function transform
    def convert(stored_doc):
        if not stored_doc:
            return
        stored_doc.update({"author": "Anonymous"})
        return stored_doc
    # remember, updates only affect a single document unless multi=True
    q = {"author":{"$regex":"^Jordan"}}
    store.update("posts", q, convert, multi=True)

    # remember, if you are querying a natural field, use_natural=True
    assert(len(store.find("posts", {"author":"Anonymous"}, use_natural=False)) == 0)
    assert(len(store.find("posts", {"author":"Anonymous"}, use_natural=True)) == 2)


def test_update_upsert():
    store = create_storage()
    assert(len(store.retrieve_all("posts")) == 0)

    def convert(stored_doc=None):
        if stored_doc is None:
            stored_doc = {}
        stored_doc.update({"title": "a",
            "test":stored_doc.get("test", 0) + 1})
        return stored_doc

    store.update("posts", {"title":"a"}, convert)
    assert(len(store.retrieve_all("posts")) == 0)

    a = store.update("posts", {"title":"a"}, convert, upsert=True)
    assert(a)
    assert(len(store.retrieve_all("posts")) == 1)

    store.update("posts", {"title":"a"}, convert)
    assert(len(store.retrieve_all("posts")) == 1)
    assert(store.retrieve(a["_uid"])["test"] == 2)


def test_patch():
    store = create_storage()
    a = store.insert("test", {"a":123, "new":False})
    b = store.insert("test", {"a":123, "b":567, "new":False})
    
    # replace the matching document with a new document value
    c = store.update("test", {"a":123}, {"new":True})
    assert(c["new"] == True)
    assert("a" not in c)

    # go back to the original document
    d = store.update("test", {"_uid":c["_uid"]}, {"a":123})
    assert(d["a"] == 123)
    assert("new" not in d)

    # incrementally update the document with a set operation
    e = store.update("test", {"a":123}, {"$set":{"new":True}})
    print(e)
    assert("a" in e and e["a"] == 123)
    assert("new" in e and e["new"] == True)

    uid = a["_uid"]
    o = store.retrieve(uid)
    assert(o["a"] == 123)
    assert("b" not in o)
    assert(o["new"] == True)

    result = store.update_document(o, {"new":False})

    x = store.retrieve(uid)
    assert(x["new"] == False)


def test_multi_index():
    store = create_storage()

    store.index_ensure("posts", fields="tags")

    store.insert("posts", {"file":".1.", "tags":["awesome", "dummy"]})
    store.insert("posts", {"file":".2.", "tags":["dummy"]})

    assert(len(store.retrieve_all("posts")) == 2)
    assert(len(store.find("posts", {"tags":"dummy"})) == 2)

    try:
        # we are expecting this to fail due to the cartesian product constraint 
        store.index_ensure("posts", fields=("tags", "categories"))
        store.insert("posts", {"tags":["a","b"], "categories":["x", "y"]})
        assert(False)
    except nosqlite.IndexError:
        assert(True)


def test_expression():
    store = create_storage()

    store.index_ensure("posts", fields="title")
    a = store.insert("posts", {"title":"a"})
    b = store.insert("posts", {"title":"b"})
    c = store.insert("posts", {"title":"c", "new":True, "author":{"fname":"Jordan"}})
    c = store.insert("posts", {"title":"d", "new":True, "author":{"fname":"Jordan"}})

    p = store.find("posts", {"title":{"$gt":"a", "$lte":"c"}}, use_natural=True)
    assert(len(p) == 2)

    p = store.find("posts", '{"title":{"$gt":"a", "$lte":"c"}}', use_natural=True)
    assert(len(p) == 2)

    q = store.find("posts", {"$or":[{"title":{"$lt":"b"}}, {"new":True}]}, use_natural=True)
    assert(len(q) == 3)
    assert(q[0]["title"] == "a")

    x = store.find("posts", {"title":{"$gt":"a"}}, sort={"title":-1}, use_natural=True)
    assert(x)
    assert(len(x) == 3)
    assert(x[0]["title"] == "d")

    q = {"$or":[ {"title":{"$lt":"c"}}, {"new":True, "title":{"$regex":"^d"}} ]}
    t = store.find("posts", q, use_natural=True)
    assert(t)
    assert(len(t) == 3)

    # make sure we can query nested fields and return them too.
    z = store.find("posts", {"author.fname":"Jordan"}, fields="author.fname", use_natural=True)
    assert(z)
    assert(len(z) == 2)
    assert("title" not in z[0])
    assert("title" not in z[1])
    assert(z[0]["author"]["fname"] == "Jordan")

    store.insert("posts", {"test":123})
    w = store.find("posts", {"test":123}, use_natural=True)
    assert(w)
    assert(len(w) == 1)

    try:
        y = store.find("posts", {"title":{"$regex":"b$"}})
        assert(False)
    except:
        assert(True)


def test_mongo_query():
    store = create_storage()

    store.index_ensure("posts", fields="title")
    a = store.insert("posts", {"title":"a"})
    b = store.insert("posts", {"title":"b"})
    c = store.insert("posts", {"title":"c", "new":True, "author":{"fname":"Jordan"}})
    c = store.insert("posts", {"title":"d", "new":True, "author":{"fname":"Jordan"}})
    
    assert(len(store.find("posts")) == 4)

    p = store.find("posts", {"title":{"$gt":"a", "$lte":"c"}}, use_natural=True)
    assert(len(p) == 2)
    
    q = store.find("posts", {"$or":[{"title":{"$lt":"b"}}, {"new":True}]}, use_natural=True)
    assert(len(q) == 3)
    assert(q[0]["title"] == "a")

    x = store.find("posts", {"title":{"$gt":"a"}}, sort='{"title":-1}', use_natural=True)
    assert(x)
    assert(len(x) == 3)
    assert(x[0]["title"] == "d")

    x = store.find("posts", {"title":{"$gt":"a"}}, sort={"title":-1}, use_natural=True)
    assert(x)
    assert(len(x) == 3)
    assert(x[0]["title"] == "d")


def test_atomic():
    store = create_storage()

    a = store.insert("posts", {"title":"a"})
    b = dict(a)

    a["new"] = True
    store.save_document(a)

    c = store.retrieve(a["_uid"])
    assert(c)
    assert(c["title"] == "a")
    assert(c["new"] == True)

    store.update_document(b, {"$set":{"new":False}})

    # this update should fail when atomic = True because version numbers do not match
    
    d = store.retrieve(a["_uid"])
    assert(d)
    assert(d["title"] == "a")
    assert(d["new"] == True)

    store.update_document(b, {"$set":{"new":False}}, atomic=False)
    e = store.retrieve(a["_uid"])
    assert(e)
    assert(e["title"] == "a")
    assert(e["new"] == False)

