"""
NoSQLite REST Test: Tests for the REST interface to NoSQLite using bottle.py

Copyright (c) 2012, Jordan Sherer
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""

__version__   = '0.0'
__author__    = 'Jordan Sherer <jordan@widefido.com>'
__license__   = 'BSD2'


import json
from nose.exc import SkipTest

import requests


base = "http://localhost:8087/"

def url(*args):
    return base + "/".join(args)


def test_get_dbs():
    r = requests.get(url())
    assert(r.status_code == 200)
    assert(r.json)
    assert("dbs" in r.json)
    assert(isinstance(r.json["dbs"], list))


def test_create_and_delete_db():
    r = requests.post(url("?db=nose"))
    assert(r.status_code == 200)
    assert(r.json)
    assert(r.json["name"])
    assert(r.json["name"] == "nose")

    r = requests.post(url())
    assert(r.status_code == 200)
    assert(r.json)
    assert(r.json["name"])

    temp_db = r.json["name"]

    r = requests.get(url())
    assert(r.status_code == 200)
    assert(r.json)
    assert("dbs" in r.json)
    assert(isinstance(r.json["dbs"], list))
    assert(temp_db in r.json["dbs"])
    assert("nose" in r.json["dbs"])

    r = requests.delete(url(temp_db))
    assert(r.status_code == 200)
    assert(r.json)
    assert(r.json["success"] == True)

    r = requests.get(url())
    assert(r.status_code == 200)
    assert(r.json)
    assert("dbs" in r.json)
    assert(isinstance(r.json["dbs"], list))
    assert(temp_db not in r.json["dbs"])


def test_get_collections():
    r = requests.get(url("nose"))
    assert(r.status_code == 200)
    assert(r.json)
    assert("collections" in r.json)


def test_create_and_delete_collection():
    r = requests.post(url("nose?collection=docs"))
    assert(r.status_code == 200)
    assert(r.json)
    assert(r.json["success"])

    r = requests.get(url("nose"))
    assert(r.status_code == 200)
    assert(r.json)
    assert("collections" in r.json)
    assert("docs" in r.json["collections"])

    r = requests.delete(url("nose", "docs"))
    assert(r.status_code == 200)
    assert(r.json)
    assert(r.json["success"])

    r = requests.get(url("nose"))
    assert(r.status_code == 200)
    assert(r.json)
    assert("collections" in r.json)
    assert("docs" not in r.json["collections"])


def test_create_and_get_document():
    doc = json.dumps({"title":"Hello World"})
    r = requests.post(url("nose", "docs"), data=doc, headers={"Content-Type":"application/json"})
    assert(r.status_code == 200)
    assert(r.json)
    assert(r.json["_uid"])
    doc_saved = r.json

    r = requests.get(url("nose", "docs", doc_saved["_uid"]))
    assert(r.status_code == 200)
    assert(r.json)
    assert(r.json["title"] == "Hello World") 

    r = requests.get(url("nose", "docs"))
    assert(r.status_code == 200)
    assert(r.json)
    assert(r.json["count"])
    assert(r.json["results"])
    assert(len(r.json["results"]) == r.json["count"])
    assert(r.json["results"][0]["_uid"] == doc_saved["_uid"])
    assert(r.json["results"][0]["title"] == "Hello World")


def test_update_document():
    doc = json.dumps({"title":"Testing 123"})
    r = requests.post(url("nose", "docs_update"), data=doc, headers={"Content-Type":"application/json"})
    assert(r.status_code == 200)
    assert(r.json)
    assert(r.json["_uid"])
    doc_saved = r.json

    doc = json.dumps({"name":"Testing 456", "new":True})
    r = requests.put(url("nose", "docs_update", doc_saved["_uid"]), data=doc, headers={"Content-Type":"application/json"})
    assert(r.status_code == 200)
    assert(r.json)
    assert(r.json["_uid"] == doc_saved["_uid"])
    assert(r.json["name"] == "Testing 456")
    assert(r.json["new"] == True)
    assert("title" not in r.json)
    doc_updated = r.json

    r = requests.get(url("nose", "docs_update", doc_saved["_uid"]))
    assert(r.status_code == 200)
    assert(r.json)
    assert(r.json["name"] == "Testing 456")
    assert(r.json["new"] == True)  
    assert("title" not in r.json)


#def test_patch_document():
#    doc = json.dumps({"title":"Testing 123"})
#    r = requests.post(url("nose", "docs_patch"), data=doc, headers={"Content-Type":"application/json"})
#    assert(r.status_code == 200)
#    assert(r.json)
#    assert(r.json["_uid"])
#    doc_saved = r.json
#
#    doc = json.dumps({"name":"Testing 456", "new":True})
#    r = requests.patch(url("nose", "docs_patch", doc_saved["_uid"]), data=doc, headers={"Content-Type":"application/json"})
#    assert(r.status_code == 200)
#    assert(r.json)
#    assert(r.json["_uid"] == doc_saved["_uid"])
#    assert("title" in r.json)
#    assert(r.json["title"] == "Testing 123")
#    assert(r.json["name"] == "Testing 456")
#    assert(r.json["new"] == True)
#    doc_updated = r.json
#
#    r = requests.get(url("nose", "docs_patch", doc_saved["_uid"]))
#    assert(r.status_code == 200)
#    assert(r.json)
#    assert(r.json["title"] == "Testing 123")
#    assert(r.json["name"] == "Testing 456")
#    assert(r.json["new"] == True)  


def test_delete_document():
    doc = json.dumps({"title":"Testing 123"})
    r = requests.post(url("nose", "docs_delete"), data=doc, headers={"Content-Type":"application/json"})
    assert(r.status_code == 200)
    assert(r.json)
    assert(r.json["_uid"])
    doc_saved = r.json

    r = requests.get(url("nose", "docs_delete", doc_saved["_uid"]))
    assert(r.status_code == 200)
    assert(r.json)
    assert(r.json["title"] == "Testing 123")
    
    r = requests.delete(url("nose", "docs_delete", doc_saved["_uid"]))
    assert(r.status_code == 200)
    assert(r.json)
    assert(r.json["success"] == True)

    r = requests.get(url("nose", "docs_delete", doc_saved["_uid"]))
    assert(r.status_code == 404)
    assert(r.json)
    assert(r.json["message"] == "document `{0}` not found".format(doc_saved["_uid"]))

    r = requests.get(url("nose", "docs_delete"))
    assert(r.status_code == 200)
    assert(r.json)
    assert(r.json["count"] == 0)


def test_find_documents():
    docs = [{"title":"aa", "author":"Jordan"}, 
            {"title":"bb", "author":"Hillary"}, 
            {"title":"cc", "author":"Fred"}, 
            {"title":"dd", "author":"Wilma"}]

    for doc in docs:
        r = requests.post(url("nose", "docs_find"), data=json.dumps(doc), headers={"Content-Type":"application/json"})
        assert(r.status_code == 200)

    r = requests.get(url("nose", "docs_find"))
    assert(r.status_code == 200)
    assert(r.json)
    assert(r.json["count"] == 4)
    assert(r.json["results"][0]["title"] == "aa")    
    assert(r.json["results"][1]["title"] == "bb")    
    assert(r.json["results"][2]["title"] == "cc")    
    assert(r.json["results"][3]["title"] == "dd")    

    r = requests.get(url("nose", "docs_find?limit=1"))
    assert(r.status_code == 200)
    assert(r.json)
    assert(r.json["count"] == 1)
    assert(r.json["results"][0]["title"] == "aa")

    r = requests.get(url("nose", "docs_find?fields=author&limit=1"))
    assert(r.status_code == 200)
    assert(r.json)
    assert(r.json["count"] == 1)
    assert(r.json["results"][0]["author"] == "Jordan")
    assert("title" not in r.json["results"][0])

    r = requests.get(url("nose", "docs_find?limit=1&skip=1"))
    assert(r.status_code == 200)
    assert(r.json)
    assert(r.json["count"] == 1)
    assert(r.json["results"][0]["title"] == "bb")

    r = requests.get(url("nose", """docs_find?q={"title":"aa"}&use_natural=false"""))
    assert(r.status_code == 200)
    assert(r.json)
    assert(r.json["count"] == 0)

    r = requests.get(url("nose", """docs_find?q={"title":"aa"}&use_natural=true"""))
    assert(r.status_code == 200)
    assert(r.json)
    assert(r.json["count"] == 1)
    assert(r.json["results"][0]["author"] == "Jordan")

    r = requests.get(url("nose", """docs_find?q={"author":{"$regex":"a"}}&limit=2&use_natural=false"""))
    assert(r.status_code == 200)
    assert(r.json)
    assert(r.json["count"] == 0)

    r = requests.get(url("nose", """docs_find?q={"author":{"$regex":"a"}}&use_natural=true&limit=2"""))
    assert(r.status_code == 200)
    assert(r.json)
    assert(r.json["count"] == 2)
    assert(r.json["results"][0]["author"] == "Jordan")
    assert(r.json["results"][1]["author"] == "Hillary")


def test_get_and_ensure_collection_indexes():
    r = requests.get(url("nose", "docs_find", "indexes"))
    assert(r.status_code == 404)
    assert(r.json)
    assert(r.json["message"] == "collection `docs_find` has no indexes")

    r = requests.post(url("nose", "docs_find", "indexes?fields=title&unique=true"))
    assert(r.status_code == 200)
    assert(r.json)

    index = r.json
    assert(index["collection"] == "docs_find")
    assert(index["key"] == "uniqueindex:docs_find:title")
    assert(index["sparse"] == False)
    assert(index["unique"] == True)
    assert(index["fields"] == ["title"])

    r = requests.post(url("nose", "docs_find", "indexes?fields=author&reindex_all=true"))
    assert(r.status_code == 200)
    assert(r.json)


def test_find_indexed_documents():
    r = requests.get(url("nose", """docs_find?q={"author":"Jordan"}"""))
    assert(r.status_code == 200)
    assert(r.json)
    assert(r.json["count"] == 1)
    assert(r.json["results"][0]["title"] == "aa")

    r = requests.get(url("nose", """docs_find?q={"author":{"$regex":"[a]"}}"""))
    assert(r.status_code == 200)
    assert(r.json)
    assert(r.json["count"] == 3)
    assert(r.json["results"][0]["author"] == "Jordan")
    assert(r.json["results"][1]["author"] == "Hillary")
    assert(r.json["results"][2]["author"] == "Wilma")


def test_reindex_collection_indexes():
    r = requests.get(url("nose", """docs_find?q={"title":"ee"}"""))
    assert(r.status_code == 200)
    assert(r.json)
    assert(r.json["count"] == 0)
    
    r = requests.put(url("nose", "docs_find", "indexes"))
    assert(r.status_code == 200)
    assert(r.json)
    assert(r.json["success"] == True)

    r = requests.post(url("nose", "docs_find"), data=json.dumps({"title":"aa"}), headers={"Content-Type":"application/json"})
    assert(r.status_code == 500)
    assert(r.json)
    assert(r.json["message"] == "Failed to index document: column title is not unique")


def test_find_reindexed_documents():
    r = requests.get(url("nose", """docs_find?q={"title":"aa"}"""))
    assert(r.status_code == 200)
    assert(r.json)
    assert(r.json["count"] == 1)
    assert(r.json["results"][0]["author"] == "Jordan")

    r = requests.get(url("nose", """docs_find?q={"title":{"$regex":"[bc]"}}"""))
    assert(r.status_code == 200)
    assert(r.json)
    assert(r.json["count"] == 2)
    assert(r.json["results"][0]["author"] == "Hillary")
    assert(r.json["results"][1]["author"] == "Fred")


def test_delete_collection_indexes():
    r = requests.delete(url("nose", "docs_find", "indexes"))
    assert(r.status_code == 200)
    assert(r.json)
    assert(r.json["success"] == True)

    r = requests.get(url("nose", """docs_find?q={"title":"aa"}&use_natural=false"""))
    assert(r.status_code == 200)
    assert(r.json)
    assert(r.json["count"] == 0)
    
    r = requests.get(url("nose", """docs_find?q={"author":"Hillary"}"""))
    assert(r.status_code == 200)
    assert(r.json)
    assert(r.json["count"] == 1)


def test_get_indexes():
    r = requests.get(url("nose", "indexes"))
    assert(r.status_code == 404)

    r = requests.post(url("nose", "docs_find", "indexes?fields=title&unique=true"))
    assert(r.status_code == 200)
    assert(r.json)
    index = r.json

    r = requests.get(url("nose", "indexes"))
    assert(r.status_code == 200)
    assert(r.json)
    assert(index["key"] in r.json)
    assert(r.json[index["key"]] == index)


def test_get_index():
    r = requests.get(url("nose", "indexes", "uniqueindex:docs_find:title"))
    assert(r.status_code == 200)
    assert(r.json)
    assert(r.json["key"] == "uniqueindex:docs_find:title")
    assert(r.json["fields"] == ["title"])


def test_delete_index():
    r = requests.delete(url("nose", "indexes", "uniqueindex:docs_find:title"))
    assert(r.status_code == 200)
    assert(r.json)
    assert(r.json["success"] == True)

    r = requests.get(url("nose", "indexes", "uniqueindex:docs_find:title"))
    assert(r.status_code == 404)


def test_cleanup():
    r = requests.delete(url("nose"))
    assert(r.status_code == 200)
    assert(r.json)
