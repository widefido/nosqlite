"""
NoSQLite Where Test: Tests for the WhereClause interface to NoSQLite

Copyright (c) 2012, Jordan Sherer
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""

from nosqlite import WhereClause

def test_where():
    sql, vals = WhereClause({"title":123}).sql()
    assert(sql == "title = $0")
    assert(len(vals) == 1)
    assert(vals[0] == 123)

    sql, vals = WhereClause({"title":{"$regex":"^T"}}).sql()
    assert(sql == "title REGEXP $0")
    assert(len(vals) == 1)
    assert(vals[0] == "^T")


def test_where_or():
    sql, vals = WhereClause({"$or":[{"title":123}, {"new":True}]}).sql()
    assert(len(vals) == 2)
    assert(vals[0] == 123)
    assert(vals[1] == True)
    assert(sql == "((title = $0) OR (new = $1))")
