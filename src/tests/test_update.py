import update

from nosqlite import Dot


def test_set():
    a = {}
    update._set(a, "title", "Hello World")
    assert(a["title"] == "Hello World")
    update._set(a, "title", "Bob")
    assert(a["title"] == "Bob")

    a = Dot()
    update._set(a, "meta.author", "Jordan")
    assert("meta" in a)
    assert("author" in a["meta"])
    assert(a["meta"]["author"] == "Jordan")


def test_unset():
    a = {"title":"Hello World"}
    update._unset(a, "title", None)
    assert("title" not in a)

    a = Dot({"meta":{"author":"Jordan", "new":True}})
    update._unset(a, "meta.author", None)
    assert("meta" in a)
    assert("author" not in a["meta"])
    assert("new" in a["meta"])


def test_inc():
    a = {}
    update._inc(a, "count", 10)
    assert(a["count"] == 10)
    update._inc(a, "count", 10)
    assert(a["count"] == 20)
    update._inc(a, "count", -1)
    assert(a["count"] == 19)


def test_rename():
    a = {"nmae":"Jordan"}
    update._rename(a, "nmae", "name")
    assert("name" in a)
    assert("nmae" not in a)
    assert(a["name"] == "Jordan")

    update._rename(a, "title", "real_title")
    assert("title" not in a)
    assert("real_title" not in a)


def test_push():
    a = {"title":"Jordan"}
    update._push(a, "tags", "Test")
    assert("tags" in a)
    assert(len(a["tags"]) == 1)

    update._push(a, "title", "Bob")
    assert(len(a) == 2)
    assert(a["title"] == "Jordan")
    assert(a["tags"] == ["Test"])


def test_push_all():
    a = {"title":"Jordan"}
    update._push_all(a, "tags", "Test")
    assert("tags" in a)
    assert(len(a["tags"]) == 1)

    update._push_all(a, "title", ["a", 2, 3])
    update._push_all(a, "tags", ["a", 2, 3])
    assert(len(a) == 2)
    assert(a["title"] == "Jordan")
    assert(a["tags"] == ["Test", "a", 2, 3])


def test_pop():
    a = {"title":"Hello World", "tags":["new", "test", "unread", "popular"]}

    update._pop(a, "title", 1)
    assert(a["title"] == "Hello World")

    update._pop(a, "test", -1)
    assert(len(a) == 2)
    assert(a["title"] == "Hello World")
    assert(len(a["tags"]) == 4)

    update._pop(a, "tags", 1)
    assert(len(a["tags"]) == 3)
    assert(a["tags"] == ["new", "test", "unread"])

    update._pop(a, "tags", -1)
    assert(len(a["tags"]) == 2)
    assert(a["tags"] == ["test", "unread"])

    update._pop(a, "tags", 0)
    assert(len(a["tags"]) == 1)
    assert(a["tags"] == ["unread"])

    update._pop(a, "tags", 0)
    assert(len(a["tags"]) == 0)
    assert(a["tags"] == [])

    update._pop(a, "tags", 0)
    assert(len(a["tags"]) == 0)
    assert(a["tags"] == [])


def test_pull():
    a = {"title":"Hello world", "tags":[1,1,2,2,3]}

    update._pull(a, "tags", 1)
    assert(len(a["tags"]) == 3)
    assert(a["tags"] == [2,2,3])

    b = a.copy()
    update._pull(a, "bob", 1)
    assert(a == b)

    update._pull(a, "tags", 2)
    assert(len(a["tags"]) == 1)

    update._pull(a, "tags", 3)
    assert(len(a["tags"]) == 0)


def test_pull_all():
    a = {"title":"Hello world", "tags":[1,1,2,2,3]}

    update._pull_all(a, "tags", 1)
    assert(len(a["tags"]) == 3)
    assert(a["tags"] == [2,2,3])

    b = a.copy()
    update._pull_all(a, "bob", 1)
    assert(a == b)

    update._pull_all(a, "tags", [2,3,3])
    assert(len(a["tags"]) == 0)


def test_add_to_set():
    a = {"title":"Hello World"}
    
    b = a.copy()
    update._add_to_set(a, "title", "test")
    assert(a == b)

    update._add_to_set(a, "tags", "new")
    assert("tags" in a)
    assert(a["tags"] == ["new"])

    update._add_to_set(a, "tags", "new")
    assert("tags" in a)
    assert(a["tags"] == ["new"])

    update._add_to_set(a, "tags", "popular")
    assert("tags" in a)
    assert(a["tags"] == ["new", "popular"])

    update._add_to_set(a, "tags", [1,2,3])
    assert("tags" in a)
    assert(a["tags"] == ["new", "popular", [1,2,3]])


def test_add_all_to_set():
    a = {"title":"Hello World"}
    
    b = a.copy()
    update._add_all_to_set(a, "title", "test")
    assert(a == b)

    update._add_all_to_set(a, "tags", "new")
    assert("tags" in a)
    assert(a["tags"] == ["new"])

    update._add_all_to_set(a, "tags", ["new", "popular"])
    assert("tags" in a)
    assert(a["tags"] == ["new", "popular"])

    update._add_all_to_set(a, "tags", ["a", "a", "a"])
    assert("tags" in a)
    assert(a["tags"] == ["new", "popular", "a"])


def test_update_query():
    a = {}

    update.update_document(a, {"$set":{"title":"Hello World", "author":"Jordan"}, "$push":{"tags":"new"}})

    assert("title" in a)
    assert("author" in a)
    assert("tags" in a)
    assert(a["title"] == "Hello World")
    assert(a["author"] == "Jordan")
    assert(a["tags"] == ["new"])

    update.update_document(a, {"test":123})
    assert("title" not in a)
    assert("author" not in a)
    assert("tags" not in a)
    assert(a["test"] == 123)

    try:
        update.update_document(a, {"test":456, "$pop":{"tags":0}})
        assert(False)
    except:
        assert(True)

