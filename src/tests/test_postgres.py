import nosqlgre

storage = None
def create_storage():
    global storage
    if storage is None:
        storage = nosqlgre.Storage("postgresql+psycopg2://jordan:password@localhost:5432/jordan", recreate=True, autoconnect=True)
    else:
        storage.database.close()
        storage.database.recreate = True
        storage.database.connect()
        pass
    return storage

import test
test.create_storage = create_storage
from test import *

import test_hierarchy
test_hierarchy.create_storage = create_storage
from test_hierarchy import *
