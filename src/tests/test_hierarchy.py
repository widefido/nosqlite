from nosqlite import Storage

def _test_hierarchy():
    s = Storage()
    s.hierarchy("posts", entry="...")                                # retrieves a hierarchy on the "posts" collection starting at entry "..."
    s.hierarchy_insert("posts", entry="...", parent="...", order=10) # inserts a new item into the hierarchy
    s.hierarchy_remove("posts", entry="...")                         # removes a subtree starting at and including entry "..."
    s.hierarchy_move("posts", entry="...", parent="...")             # moves a subtree starting at and including entry "..." to a new parent "..."
    #s.hierarchy_refresh("posts", "parent_uid")                       # creates/updates a hierarchy on the "posts" collection. parent relationship is defined by the key "parent_uid" that should appear on the object.

def create_storage():
    return Storage()


def test_storage_hierarchy():
    s = create_storage()
    a = s.insert("posts", {"title":"hello"})
    b1 = s.insert("posts", {"_parent_uid":a["_uid"], "title":"b1"})
    b2 = s.insert("posts", {"_parent_uid":a["_uid"], "title":"b2"})
    c1 = s.insert("posts", {"_parent_uid":b1["_uid"], "title":"c1"})
    c2 = s.insert("posts", {"_parent_uid":b2["_uid"], "title":"c2"})


    root, tree = s.retrieve_hierarchy(a["_uid"])

    assert(root == a["_uid"])

    assert(len(tree) == 3)
    assert(len(tree[root]) == 2)

    t1 = tree[root][0]
    t2 = tree[root][1]
    assert(t1.child == b1["_uid"])
    assert(t2.child == b2["_uid"])

    assert(len(tree[t1.child]) == 1)
    assert(len(tree[t2.child]) == 1)

    t11 = tree[t1.child][0]
    t21 = tree[t2.child][0]

    assert(t11.child == c1["_uid"])
    assert(t21.child == c2["_uid"])


def test_storage_hierarchy_ancestors():
    s = create_storage()
    a = s.insert("posts", {"title":"hello"})
    b1 = s.insert("posts", {"_parent_uid":a["_uid"], "title":"b1"})
    b11 = s.insert("posts", {"_parent_uid":b1["_uid"], "title":"b11"})
    b111 = s.insert("posts", {"_parent_uid":b11["_uid"], "title":"b111"})
    c1 = s.insert("posts", {"_parent_uid":a["_uid"], "title":"c1"})

    root, ancestors = s.retrieve_ancestors(b1["_uid"])
    assert(root == b1["_uid"])
    assert(len(ancestors) == 2)
    assert(ancestors[0].child == b1["_uid"])
    assert(ancestors[1].child == a["_uid"])

    root, ancestors = s.retrieve_ancestors(b111["_uid"])
    assert(root == b111["_uid"])

    assert(len(ancestors) == 4)
    assert(ancestors[0].child == b111["_uid"])
    assert(ancestors[1].child == b11["_uid"])
    assert(ancestors[2].child == b1["_uid"])
    assert(ancestors[3].child == a["_uid"])


def test_storage_hierarchy_delete():
    s = create_storage()
    a = s.insert("posts", {"title":"hello"})
    b1 = s.insert("posts", {"_parent_uid":a["_uid"], "title":"b1"})
    b2 = s.insert("posts", {"_parent_uid":a["_uid"], "title":"b2"})
    c1 = s.insert("posts", {"_parent_uid":b1["_uid"], "title":"c1"})
    c2 = s.insert("posts", {"_parent_uid":b2["_uid"], "title":"c2"})

    
    c1["_parent_uid"] = None
    s.save_document(c1)

    root, tree = s.retrieve_hierarchy(a["_uid"])
    
    assert(root == a["_uid"])

    assert(len(tree) == 2)
    assert(len(tree[root]) == 2)

    t1 = tree[root][0]
    t2 = tree[root][1]
    assert(t1.child == b1["_uid"])
    assert(t2.child == b2["_uid"])

    # assert that t1 (b1) has no children
    assert(t1.child not in tree)
    
    assert(len(tree[t2.child]) == 1)

    t21 = tree[t2.child][0]

    assert(t21.child == c2["_uid"])


def test_storage_hierarchy_update():
    s = create_storage()    
    a = s.insert("posts", {"title":"hello"})
    b1 = s.insert("posts", {"_parent_uid":a["_uid"], "title":"b1"})
    b2 = s.insert("posts", {"_parent_uid":a["_uid"], "title":"b2"})
    c1 = s.insert("posts", {"_parent_uid":b1["_uid"], "title":"c1"})
    c2 = s.insert("posts", {"_parent_uid":b2["_uid"], "title":"c2"})

    
    c1["_parent_uid"] = b2["_uid"]
    s.save_document(c1)

    root, tree = s.retrieve_hierarchy(a["_uid"])
    
    assert(root == a["_uid"])

    assert(len(tree) == 2)
    assert(len(tree[root]) == 2)

    t1 = tree[root][0]
    t2 = tree[root][1]
    assert(t1.child == b1["_uid"])
    assert(t2.child == b2["_uid"])

    # assert that t1 (b1) has no children
    assert(t1.child not in tree)
    
    assert(len(tree[t2.child]) == 2)

    t21 = tree[t2.child][0]
    t22 = tree[t2.child][1]

    assert(t21.child == c2["_uid"])
    assert(t22.child == c1["_uid"])


def test_storage_hierarchy_move():
    s = create_storage()    
    a = s.insert("posts", {"title":"hello"})
    b1 = s.insert("posts", {"_parent_uid":a["_uid"], "title":"b1"})
    b11 = s.insert("posts", {"_parent_uid":b1["_uid"], "title":"b11"})
    b111 = s.insert("posts", {"_parent_uid":b11["_uid"], "title":"b111"})
    c1 = s.insert("posts", {"_parent_uid":a["_uid"], "title":"c1"})

    root, tree = s.retrieve_hierarchy(a["_uid"])
    
    assert(root == a["_uid"])

    assert(len(tree) == 3)
    assert(len(tree[root]) == 2)

    tb1 = tree[root][0]
    tc1 = tree[root][1]
    assert(tb1.child == b1["_uid"])
    assert(tc1.child == c1["_uid"])

    assert(len(tree[tb1.child]) == 1)
    assert(tree[tb1.child][0].child == b11["_uid"])
    assert(len(tree[tree[tb1.child][0].child]) == 1)
    assert(tree[tree[tb1.child][0].child][0].child == b111["_uid"])

    b11["_parent_uid"] = a["_uid"]
    s.save_document(b11)

    root, tree = s.retrieve_hierarchy(a["_uid"])

    assert(len(tree) == 2)

    assert(len(tree[root]) == 3)
    assert(tree[root][-1].child == b11["_uid"])
