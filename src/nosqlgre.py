# -*- coding: utf-8 -*-

"""
NoSQLgre: A "nosql" implementation using json and postgres.

Copyright (c) 2012, Jordan Sherer
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""

__version__      = "0.0"
__author__       = "Jordan Sherer"
__author_email__ = "jordan@widefido.com"
__license__      = "BSD2"
__url__          = "http://bitbucket.org/widefido/nosqlite"

import re 
import sys
import uuid

import nosqlite

import sqlalchemy
import sqlalchemy.exc
import sqlalchemy.ext.compiler

import psycopg2
import psycopg2.extensions


if sys.version_info >= (3,0):
    basestring = str


class UUIDAdapter(object):
    def __init__(self, uuid):
        self.uuid = uuid

    def getquoted(self):
        return "'{0}'".format(self.uuid.hex)


psycopg2.extensions.register_adapter(uuid.UUID, UUIDAdapter)


class Database(nosqlite.Database):
    """
    Wrapper around a SQL database connection, allowing for nested contexts,
    tracing, and other helper functionality.

    """
    def __init__(self, path, autoconnect=True, trace=False, 
            recreate=False, on_new=None):

        self.engine = sqlalchemy.create_engine(path)
        self.metadata = sqlalchemy.MetaData(bind=self.engine)
        self.recreate = recreate
        self.transactions = []
        
        nosqlite.Database.__init__(self, path, autoconnect=autoconnect, 
            on_new=on_new, trace=trace)

        if self.trace:
            self.engine.echo = True


    def __enter__(self):
        """
        Use the database context wrapper for allowing the database
        to be used with the with operator:

        >>> d = Database()
        >>> with d:
        ...     d.execute(...)   

        """ 
        self.transactions.append(self.connection.begin())
        return self


    def __exit__(self, exc_type, exc_value, traceback):
        """
        Use the database context wrapper for allowing the database
        to be used with the with operator:

        >>> d = Database()
        >>> with d:
        ...     d.execute(...)   

        """
        transaction = None
        if self.transactions:
            transaction = self.transactions.pop()

        if not transaction:
            pass

        if exc_type is not None:
            transaction.rollback()
        else:
            transaction.commit()


    def connect(self):
        """
        Connect to the database provided in path in the constructor

        """
        self.connection = self.engine.connect()
        
        if self.recreate:
            self.metadata = sqlalchemy.MetaData(bind=self.engine)
            self.execute("DROP TABLE IF EXISTS nosqlite CASCADE;")
            self.recreate = False
        
        self.test_new()

    
    def commit(self, *args, **kwargs):
        if hasattr(self.engine, "commit"):
            self.engine.commit()
        pass


    def test_new(self):
        """
        Test if this is a new Database. If so, create the initial schema.

        """
        if not self.table_exists("nosqlite"):
            master = sqlalchemy.Table("nosqlite", self.metadata, 
                sqlalchemy.Column("schema_version", sqlalchemy.Unicode(), primary_key=True)
            ) 
            self.metadata.create_all()

            self.execute("INSERT INTO nosqlite (schema_version) VALUES ('0.0');")

            if callable(self.on_new):
                self.on_new(self)


    def execute(self, *args, **kwargs):
        """
        Proxy to the connection.execute function

        """
        if self.trace:
            log.info(args)
        
        args = list(args)
        args[0] = transform_query(args[0])

        return self.connection.execute(*args, **kwargs)
    

    def executemany(self, *args, **kwargs):
        """
        Proxy to the connection.executemany function

        """
        args = list(args)
        args[0] = transform_query(args[0])

        return self.connection.executemany(*args, **kwargs)
    
    
    def executescript(self, *args, **kwargs):
        """
        Proxy to the connection.executescript function

        """
        args = list(args)
        args[0] = transform_query(args[0])

        return self.connection.executescript(*args, **kwargs) 

    
    def table_exists(self, table):
        """
        Return whether or not the table exists

        """
        return self.execute("select 1 from information_schema.tables where table_name=%s", (table,)).rowcount > 0


    def table_info(self, table):
        """
        Return a dictionary object of table info or None if the table does
        not exist.

        The dictionary object has two keys:
            1) column_names: a list of ordered column names
            2) column_info: a dictionary of columnar information
        
        """
        c = self.execute("SELECT ordinal_position, column_name, data_type, "
            "is_nullable, column_default FROM information_schema.columns "
            "WHERE table_name = %s ORDER BY ordinal_position DESC", (table,))
        results = c.fetchall()
        if not results:
            return None
        
        table_info = {"column_names":[], "column_info":{}}
        for result in results:
            table_info["column_names"].append(result[1])
            table_info["column_info"][result[1]] = {"type":result[2],
                "nullable":result[3],
                "default":result[4]}

        return table_info


class Storage(nosqlite.Storage):
    """
    Storage is the main nosql interface to storing documents.

    Documents are organized into groups of "collections" in the database.
    These groups can be indexed and queried, and allow for documents
    to be retrieved efficiently by their unique id (UUID4).

    Storage uses SQLite for its backend. Upon creating a new Storage object, 
    a SQLite database is either loaded or created. By specifying a path, the 
    storage object can load any SQLite database that Python can access, 
    including a database stored in memory (default). 

    """
    def __init__(self, path="", autoconnect=True, trace=False, recreate=False):
        self.database = Database(path, autoconnect=autoconnect, trace=trace,
            recreate=recreate, on_new=self._on_new)

    
    def _on_new(self, database):
        """
        Callback to create the required base tables for storage.
        
        """
        database.execute("CREATE EXTENSION IF NOT EXISTS plv8;");

        database.execute("""CREATE or REPLACE FUNCTION 
        payload_field(data text, key text) RETURNS TEXT AS $$

            var ret = JSON.parse(data); 
            var keys = key.split('.')
            var len = keys.length;

            for (var i = 0; i < len; i++) {
                if (typeof ret === "undefined"){
                    ret = null;
                    break;
                }

                ret = ret[keys[i]];
            }

            if(typeof ret === "string"){
                return ret;
            }
            return JSON.stringify(ret);

        $$ LANGUAGE plv8 IMMUTABLE STRICT;""")

        
        """
        explain analyze select * from entity where payload_field(payload, 'title') = '"Hello World"';
        explain analyze select * from entity where payload_field(payload, 'author') = '"Hello World"';
        explain analyze select * from entity where added_id < 100;
        create index entity_title on entity (payload_field(payload, 'title'));
        """


        collection = sqlalchemy.Table("collection", database.metadata, 
            sqlalchemy.Column("name", sqlalchemy.Unicode(), primary_key=True)
        )

        entity = sqlalchemy.Table("entity", database.metadata, 
            sqlalchemy.Column("added_id", sqlalchemy.Integer(), sqlalchemy.Sequence("seq_entity_added_id"), primary_key=True), 
            sqlalchemy.Column("collection", None, sqlalchemy.ForeignKey("collection.name", ondelete="CASCADE")), 
            sqlalchemy.Column("uid", sqlalchemy.Unicode(), unique=True),
            sqlalchemy.Column("created", sqlalchemy.Float()),
            sqlalchemy.Column("updated", sqlalchemy.Float()),
            sqlalchemy.Column("payload", sqlalchemy.UnicodeText())
        )

        indexes = sqlalchemy.Table("indexes", database.metadata, 
            sqlalchemy.Column("name", sqlalchemy.Unicode(), primary_key=True), 
            sqlalchemy.Column("collection", None, sqlalchemy.ForeignKey("collection.name", ondelete="CASCADE")),
            sqlalchemy.Column("payload", sqlalchemy.UnicodeText())
        )

        database.metadata.create_all()
        
        database.execute("ALTER TABLE IF EXISTS entity ALTER COLUMN added_id SET DEFAULT NEXTVAL('seq_entity_added_id');")

        for row in database.execute("SELECT name FROM indexes").fetchall():
            index = row[0]
            database.execute("DROP INDEX IF EXISTS {0}".format(nosqlite.escape(index)))

        database.execute("DELETE FROM entity;")
        database.execute("DELETE FROM collection;")
        database.execute("DELETE FROM indexes;")

        assert(database.table_exists("collection"))
        assert(database.table_exists("entity"))
        assert(database.table_exists("indexes"))
   
    
    def index_exists(self, key):
        sql = """SELECT
        c.relname as "Name",
        c2.relname as "Table"        
        FROM pg_catalog.pg_class c
            JOIN pg_catalog.pg_index i ON i.indexrelid = c.oid
            JOIN pg_catalog.pg_class c2 ON i.indrelid = c2.oid
            LEFT JOIN pg_catalog.pg_user u ON u.usesysid = c.relowner
            LEFT JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace
        WHERE c.relkind IN ('i','')
            AND n.nspname NOT IN ('pg_catalog', 'pg_toast')
            AND pg_catalog.pg_table_is_visible(c.oid)
            AND c.relname = ?
        ORDER BY 1;
        """
        with self.database: 
            record = self.database.execute("SELECT 1 FROM indexes WHERE name=?", (key,))
            index = self.database.execute(sql, (key,))
            return record.rowcount > 0 and index.rowcount > 0


    def index_drop(self, key):
        with self.database:
            self.database.execute("DELETE FROM indexes WHERE name=?", (key,))
            self.database.execute("DROP INDEX IF EXISTS {0}".format(nosqlite.escape(key)))

    def index_create(self, collection, fields, unique=False, sparse=False,
            key=None, index_all=False):
        if not self.collection_exists(collection):
            self.collection_create(collection)
       
        if not key:
            key = _index_key(collection, fields, unique=unique, sparse=sparse)

        # Create the index object for storage
        index = nosqlite.Index(collection, fields, unique, sparse, key=key)

        # Insert an entry into the index table
        query = ("INSERT INTO indexes (name, collection, payload)"
                 "VALUES (?,?,?)")
        self.database.execute(query, (key, collection, index.to_payload()))

        nextid = self.database.execute("SELECT NEXTVAL('seq_entity_added_id')").fetchone()

        unique = "UNIQUE" if unique else ""
        columns = ", ".join(str(PayloadField(field)) for field in fields)
        values = [collection]
        where = ["collection = ?"]
        if not index_all:
            where.append("added_id > {0}".format(nextid[0]))
        if sparse:
            sparse_where = []
            for field in fields:
                sparse_where.append("{0} IS NOT NULL".format(PayloadField(field)))
            where.append("({0})".format(" OR ".join(sparse_where)))
        if where:
            where = "WHERE {0}".format(" AND ".join(where))

        sql = "DROP INDEX IF EXISTS {0}".format(nosqlite.escape(key))
        self.database.execute(sql)

        sql = "CREATE {0} INDEX {1} ON entity ({2}) {3};".format(unique, nosqlite.escape(key), columns, where)
        self.database.execute(sql, values)

        self.database.commit()
        
        return index
    
    
    def index_document(self, doc):
        """ NOOP """
        pass

    
    def index_document_with_index(self, doc, index_key, index):
        """ NOOP """
        pass


    def _generate_find_sql(self, collection, query=None, sort=None, skip=None,
            limit=None, use_natural=False):

        entity = self.database.metadata.tables["entity"]

        meta_fields_set = set(nosqlite.META_KEYS)
        query_fields = set()

        indexes = self.collection_indexes(collection)
        indexed_fields_set = set()
        indexed_fields_map = {}
        if indexes:
            for index in indexes.values():
                indexed_fields_set.update(index.fields)

                for field in index.fields:
                    if field not in indexed_fields_map:
                        indexed_fields_map[field] = [index]
                    else:
                        indexed_fields_map[field].append(index)

        s = sqlalchemy.select([entity])
        where = None

        if collection:
            where = entity.c.collection == collection

        if query:
            if isinstance(query, str):
                query = nosqlite.ordered_json_loads(query)
            
            if not isinstance(query, dict):
                raise QueryError("Query must be a dictionary")

            query_fields = set(nosqlite.fields_from_query(query))
            natural_fields = query_fields - indexed_fields_set - meta_fields_set

            if not use_natural and len(natural_fields) > 0:
                nosqlite.log.info("cannot query natural field without use_natural=True")
                return "SELECT * FROM entity WHERE 1 = 2", []

            where = sqlalchemy.and_(where, WhereClause(query, table=entity).sql())

        s = s.where(where)
        
        if not sort:
            s = s.order_by(sqlalchemy.asc(entity.c.added_id))
        else:
            if isinstance(sort, str):
                sort = nosqlite.ordered_json_loads(sort)
            
            if not isinstance(sort, dict):
                raise nosqlite.QueryError("Sort expression must be a dictionary")

            sort = [(f, "DESC" if d < 0 else "ASC") for f, d in sort.items()]
            sort_fields = set(f for f, d in sort)
            """
            #    we could be requesting a sort on a field that isn't 
            #    returned in the query, so we need to:
            #    1) see if the field has an index and if so, add the index
            #    2) otherwise, check to see if use_natural=True and if so
            #       add in the payload field as a column for sorting
            """
            unmatched_fields = ((sort_fields ^ query_fields) & sort_fields)
            unmatched_fields = ((unmatched_fields ^ indexed_fields_set) & sort_fields)
            if not use_natural and unmatched_fields:
                raise nosqlite.QueryError("Sort expression must include an " 
                            "indexed field, or use_natural must be True")

            for expr in sort:
                if len(expr) == 2:
                    f, d = expr
                elif len(expr) == 1:
                    f = expr[0]
                    d = "ASC"
                else:
                    raise nosqlite.QueryError("Invalid sort expression")

                s = s.order_by(sqlalchemy.asc(PayloadField(f)).nullslast() if d == "ASC" else
                    sqlalchemy.desc(PayloadField(f)).nullslast())

        if limit is not None:
            s = s.limit(limit)

        if skip is not None:
            s = s.offset(skip)

        s = s.compile()

        return str(s), s.params


    def _update(self, collection, stored_doc, doc, omit_payload=False):
        """
        Update an object in the collection, updating indices as well. 

        Updates are performed by replacing the contents of stored_doc with
        the contents of doc. We do this by basically updating the storage
        location where stored_doc lives with the contents of doc.

        """
        META_KEY_COLLECTION = nosqlite.META_KEY_COLLECTION
        META_KEY_UID = nosqlite.META_KEY_UID

        if META_KEY_COLLECTION not in stored_doc:
            raise KeyError("doc must be registered in collection to update")

        if (META_KEY_COLLECTION in stored_doc and 
            collection is not None and
            stored_doc[META_KEY_COLLECTION] != collection):
            raise KeyError("doc must be registered in the collection provided")
        
        if META_KEY_UID not in stored_doc:
            raise KeyError("doc must already be inserted to update")

        uid = doc[META_KEY_UID] = stored_doc[META_KEY_UID]
        doc[META_KEY_COLLECTION] = stored_doc[META_KEY_COLLECTION]

        if collection:
            self.collection_create(collection)

        if not omit_payload:
            payload = nosqlite._doc_to_payload(doc)
        else:
            payload = None
        
        with self.database:
            self.database.execute("UPDATE entity SET "
                "updated = extract(epoch from now()), payload = ? WHERE uid = ?",
                (payload, uid))
        
            self.index_document(doc)

            return self.retrieve(uid) 

    
    def insert(self, *args, **kwargs):
        try:
            return super(Storage, self).insert(*args, **kwargs)
        except sqlalchemy.exc.IntegrityError:
            pass


class WhereClause(object):
    """
    A simple MongoDB-esque query parser to generate a SQL WHERE clause from
    a dictionary of terms. Also, provides an interface to use field aliases when 
    generating the SQL.

    """
    operations = {
        "$eq":"__eq__",
        "$gt":"__gt__",
        "$lt":"__lt__", 
        "$gte":"__ge__",
        "$lte":"__le__",
        "$ne":"__ne__",
        "$regex":"regex",
        "$glob":"match",
        "$match":"match",
        "$like":"like",

        "$in":"in_",
        "$nin":"nin"
    }


    def __init__(self, query=None, table=None):
        self.query = query
        self.clauses = []
        self.binds = []
        self.keys = []
        self.field_aliases = {}
        self.count = 0
        self.table = table

    
    def clause(self, key, value, operation=None):
        clauses = []
        binds = []
        keys = []

        if operation is None:
            operation = WhereClause.operations["$eq"]
        
        if key in ("$or", "$and"):
            if not isinstance(value, list) and not isinstance(value, tuple):
                raise ValueError("value must be a list or tuple when used with $or")

            group_clauses = []
            for v in value:
                subgroup_clauses = []
                for k in v:
                    c, b, ks = self.clause(k, v[k])
                    subgroup_clauses.extend(c)
                    binds.extend(b)
                    keys.extend(ks)
                group_clauses.append(sqlalchemy.and_(*subgroup_clauses).self_group())
            clauses.append(sqlalchemy.or_(*group_clauses).self_group() if key == "$or" else sqlalchemy.and_(*group_clauses).self_group())

        elif isinstance(value, dict):
            for operation in value:
                c, b, ks = self.clause(key, value[operation], WhereClause.operations[operation])
                clauses.extend(c)
                binds.extend(b)
                keys.extend(ks)
        else:
            keys.append(key)

            binds.append(value)
            self.count += 1

            field = PayloadField(key)
            if self.table is not None and key.strip("_") in self.table.c:
                field = self.table.c[key.strip("_")]

            if operation == "nin":
                clauses.append(~field.in_(value))
            else:
                if isinstance(field, PayloadField) and not isinstance(value, basestring):
                    import json
                    value = json.dumps(value)
                clauses.append(getattr(type(field), operation)(field, value))
        return clauses, binds, keys

    
    def format_key(self, key):
        """
        Return a field alias for key if exists, otherwise return the key as is.

        """
        if not self.field_aliases:
            return key
        return self.field_aliases.get(key, key)
    

    def sql(self, field_aliases=None):
        """ 
        Returns a generated SQL string and a list of binded values from the 
        query provided in the constructor.

        """
        self.field_aliases = field_aliases

        for key in self.query:
            clauses, binds, keys = self.clause(key, self.query[key])
            self.clauses.extend(clauses)
            self.binds.extend(binds)
            self.keys.extend(keys)

        if self.clauses:
            return sqlalchemy.and_(*self.clauses)


class PayloadField(sqlalchemy.sql.expression.ColumnClause):
    def regex(self, *args, **kwargs):
        return self.op("~")(*args, **kwargs)


@sqlalchemy.ext.compiler.compiles(PayloadField)
def compile_payload_field(element, compiler, **kw):
    return "payload_field(payload, \'{0}\')".format(element.name.replace('"', '""').replace("'", "''"))


def transform_query(query):
    """
    Transform a parameterized query into a postgres compatible query

    >>> transform_query("WHERE a = ?")
    'WHERE a = %s'

    """
    transform = re.compile(r"""(["](?:""|\\"|[^"])+["])|(['](?:''|\\'|[^'])+['])|(\$\$(?:.+?)\$\$)|([?]\d*)|([$:]\w+)|(\w+)|(\s)""")

    items = [item for item in transform.finditer(query)
        if item.group(3) or item.group(4)]

    # do this in reverse so we can modify the string without having to keep
    # a list of modification offsets
    items.reverse()
    
    for item in items:
        span = item.span(3)
        if span[0] == -1:
            span = item.span(4)
        if span[0] == -1 or span[1] == -1:
            continue

        query = query[:span[0]] + "%s" + query[span[1]:]

    return query
