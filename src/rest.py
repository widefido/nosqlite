"""
NoSQLite REST: REST interface to NoSQLite using bottle.py

Copyright (c) 2012, Jordan Sherer
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""

__version__   = '0.0'
__author__    = 'Jordan Sherer <jordan@widefido.com>'
__license__   = 'BSD2'


import datetime
import glob
import json
import os
import uuid

from bottle import request, response, route, run, install, uninstall
from bottle import app, JSONPlugin

import nosqlite


class JSONEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, uuid.UUID):
            return obj.hex
        if isinstance(obj, datetime.datetime):
            return obj.isoformat()
        if isinstance(obj, nosqlite.Dot):
            return obj.value
        super(JSONEncoder, self).default(obj)


def json_dumps(o):
    return json.dumps(o, cls=JSONEncoder)


class JSONAPIPlugin(object):
    """
    Allow for callback parameters on JSON API requests
    https://gist.github.com/1454922

    """
    name = "jsonapi"
    api = 1

    def __init__(self, json_dumps=json_dumps):
        uninstall("json")
        self.json_dumps = json_dumps

    def apply(self, callback, context):
        dumps = self.json_dumps
        if not dumps: return callback
        def wrapper(*a, **ka):
            r = callback(*a, **ka)

            # Attempt to serialize, raises exception on failure
            json_response = dumps(r)

            # Wrap in callback function for JSONP
            callback_function = request.query.get("callback")
            
            # Wrap response and set content type if callback provided
            if callback_function:
                response.content_type = "text/javascript"
                json_response = "".join([callback_function, "(", json_response, ")"])

            return json_response
        return wrapper


class RESTError(nosqlite.StorageError):
    def __init__(self, message, status=500):
        super(RESTError, self).__init__(message)
        self.status = status 


def accepts(contentTypes):
    import mimeparse
    if isinstance(contentTypes, str):
        contentTypes = [contentTypes]

    def decorator(f):
        def wrapper(*args, **kwargs):
            accept =  request.headers.get('Accept')
            match = mimeparse.best_match(contentTypes, accept)
            if match  not in contentTypes:
                raise RESTError("Method does not accept `{0}`".format(accept), status=406)
            response.content_type = match
            return f(*args, **kwargs)
        return wrapper
    return decorator


def rest(f):
    ac = accepts(["text/javascript", "application/json"])

    def wrapper(*args, **kwargs):
        try:
            response.headers["Access-Control-Allow-Origin"] = "*";
            return ac(f)(*args, **kwargs)
        except RESTError as e:
            response.status = e.status
            return {"success":False, "status":e.status, "message":e.message}
        except Exception as e:
            import traceback, sys
            exc_type, exc_value, exc_traceback = sys.exc_info()
            response.status = 500
            return {"success":False, "status":500, "message":e.message, "trace":traceback.extract_tb(exc_traceback)}
    return wrapper


def database_path(name):
    db_dir = os.path.join(os.getcwd(), "databases")
    if not os.path.exists(db_dir):
        os.mkdir(db_dir)
    path = "{0}.db3".format(name)
    path = os.path.join(db_dir, path)
    return path


def prepare_routing():
    """
    Create DB

    Get collections
    Create collection
    Drop collection

    Get all items in collection (query)
    Insert item into collection
    Get item from collection by id
    Update item by id
    Delete item by id
    
    Get all indexes for collection
    Reindex all documents in collection
    Insert index for collection
    Drop index

    ---

    # DB
    GET     /                             => get_dbs(...)
    POST    /                             => create_db(...)
            ?db=<db>
    DELETE  /<db>                         => delete_db(...)

    # COLLECTIONS
    GET     /<db>                         => <db>.collections(...)
    POST    /<db>                         => <db>.collection_create(...)
            ?collection=<collection>
    DELETE  /<db>/<collection>            => <db>.collection_drop(...)

    # DOCUMENTS
    GET     /<db>/<collection>            => <db>.find(...)
            ?q=<query>
            &fields=<fields>
            &sort=<sort>
            &skip=<skip>
            &limit=<limit>
            &use_natural=<use_natural>
    POST    /<db>/<collection>            => <db>.insert(...)
            ?omit_payload=<omit_payload>
    GET     /<db>/<collection>/<id>       => <db>.retrieve(...)
    PUT     /<db>/<collection>/<id>       => <db>.update(...)
            ?upsert=<upsert>
            &multi=<multi>
            &sort=<sort>
            &skip=<skip>
            &limit=<limit>
            &omit_payload=<omit_payload>
    ####PATCH   /<db>/<collection>/<id>       => <db>.patch(...)
    ####        ?multi=<multi>
    ####        &sort=<sort>
    ####        &skip=<skip>
    ####        &limit=<limit>
    ####        &omit_payload=<omit_payload>
    DELETE  /<db>/<collection>/<id>       => <db>.remove(...)
    GET /<db>/<collection>/<uid>/hierarchy => <db>.retrieve_hierarchy(...)
    GET /<db>/<collection>/<uid>/ancestors => <db>.retrieve_ancestors(...)
    
    # INDEXES
    GET     /<db>/<collection>/indexes    => <db>.collection_indexes(...)
    DELETE  /<db>/<collection>/indexes    => <db>.collection_drop_indexes(...)
    PUT     /<db>/<collection>/indexes    => <db>.collection_reindex_indexes(...)
    POST    /<db>/<collection>/indexes    => <db>.index_ensure(...)
            ?fields=<fields> (csv)
            &unique=<unique>
            &sparse=<sparse>,
            &reindex_all=<reindex_all>
            &key=<key>
    GET     /<db>/indexes => <db>.indexes(...)
    GET     /<db>/indexes/<index> => <db>.index_retrieve(...)
    DELETE  /<db>/indexes/<index> => <db>.index_drop(...)
    """

    # DB
    route("/", method="GET", callback=get_dbs)
    route("/", method="POST", callback=create_db)
    route("/<db>", method="DELETE", callback=delete_db)

    # INDEXES
    route("/<db>/<collection>/indexes", method="GET", callback=get_collection_indexes)
    route("/<db>/<collection>/indexes", method="DELETE", callback=delete_collection_indexes)
    route("/<db>/<collection>/indexes", method="PUT", callback=reindex_collection_indexes)
    route("/<db>/<collection>/indexes", method="POST", callback=ensure_collection_index)
    route("/<db>/indexes", method="GET", callback=get_indexes)
    route("/<db>/indexes/<name>", method="GET", callback=get_index)
    route("/<db>/indexes/<name>", method="DELETE", callback=delete_index)

    # COLLECTIONS
    route("/<db>", method="GET", callback=get_collections)
    route("/<db>", method="POST", callback=create_collection)
    route("/<db>/<collection>", method="DELETE", callback=delete_collection)

    # DOCUMENTS
    route("/<db>/<collection>", method="GET", callback=get_documents)
    route("/<db>/<collection>", method="POST", callback=create_document)
    route("/<db>/<collection>/<uid>", method="GET", callback=get_document)
    route("/<db>/<collection>/<uid>", method="PUT", callback=update_document)
    #route("/<db>/<collection>/<uid>", method="PATCH", callback=patch_document)
    route("/<db>/<collection>/<uid>", method="DELETE", callback=delete_document)
    route("/<db>/<collection>/<uid>/hierarchy", method="GET", callback=get_document_hierarchy)
    route("/<db>/<collection>/<uid>/ancestors", method="GET", callback=get_document_ancestors)

    
def prepare_store(db):
    if not db:
        raise RESTError("database name not provided", status=400)
    path = database_path(db)
    if not os.path.exists(path):
        raise RESTError("database `{0}` does not exist".format(db), status=404)
    return nosqlite.Storage(path)


def to_bool(value):
    if value is None or value == "":
        return None
    return True if value.lower().strip() == "true" else False


def to_int(value):
    if value is None or value == "":
        return None
    return int(value)


def to_array(value):
    if value is None or value == "":
        return None
    return [v.strip() for v in value.split(",")]


@rest
def get_dbs():
    """
    Get all databases.

    """
    db_dir = os.path.join(os.getcwd(), "databases")
    pattern = os.path.join(db_dir, "*.db3")

    dbs = [os.path.splitext(os.path.split(path)[1])[0]
            for path in glob.glob(pattern)]
    
    return {"dbs":dbs}


@rest
def create_db():
    """
    Create a database with the provided name. Otherwise, create a new database
    with a random name and return the name of the database created.

    @param (optional) db - database name to create (default=random integer)
    
    @return {"name":"..."}

    """
    db = request.params.get("db")
    if not db:
        db = uuid.uuid4().int >> 64
    path = database_path(db)
    if os.path.exists(path):
        raise RESTError("database `{0}` already exists".format(db))
    nosqlite.Storage(path)
    if not os.path.exists(path):
        raise RESTError("database `{0}` could not be created".format(db))
    return {"name":str(db)}


@rest
def delete_db(db):
    """
    Delete a database

    """
    if not db:
        raise RESTError("database not provided", status=400)
    path = database_path(db)
    if not os.path.exists(path):
        raise RESTError("database `{0}` does not exist".format(db), status=404)
    os.remove(path)
    if os.path.exists(path):
        raise RESTError("database `{0}` could not be removed".format(db))
    return {"success":True}


@rest
def get_collections(db):
    """
    Get a list of collections for the provided database.

    """
    store = prepare_store(db)
    return {"collections":store.collections()}


@rest
def create_collection(db):
    """
    Create a collection.

    """
    store = prepare_store(db)
    collection = request.params.get("collection")
    if not collection:
        raise RESTError("collection not provided", status=400)
    store.collection_create(collection)
    return {"success":True}


@rest
def delete_collection(db, collection):
    """
    Delete a collection.

    """
    store = prepare_store(db)
    store.collection_drop(collection)
    return {"success":True}


@rest
def get_documents(db, collection):
    """
    Find documents in a collection.

    """
    query = request.query.get("q", "")
    fields = to_array(request.query.get("fields", ""))

    params = {
        "sort":        request.query.get("sort", None),
        "skip":        to_int(request.query.get("skip", "")),
        "limit":       to_int(request.query.get("limit", "")),
        "use_natural": to_bool(request.query.get("use_natural", ""))
    }

    kwargs = dict((key, value) for key, value in params.items() if value is not None)    

    store = prepare_store(db)
    results = store.find(collection, query, fields, **kwargs)
    return {"results":results, "count":len(results)}


@rest
def create_document(db, collection):
    """
    Create a document and add it to a collection.

    """
    document = request.json
    if not document:
        raise RESTError("document not provided", status=400)

    store = prepare_store(db)

    return store.insert(collection, document)


@rest
def get_document(db, collection, uid):
    """
    Get a single document

    """
    store = prepare_store(db)

    doc = store.retrieve(uid)
    if not doc:
        raise RESTError("document `{0}` not found".format(uid), status=404)

    return doc


@rest
def update_document(db, collection, uid):
    """
    Update a document

    """
    document = request.json
    if not document:
        raise RESTError("document not provided", status=400)

    omit_payload = to_bool(request.params.get("omit_payload", ""))
    upsert = to_bool(request.params.get("upsert", ""))
    atomic = to_bool(request.params.get("atomic", ""))

    store = prepare_store(db)

    return store.update(collection, {nosqlite.META_KEY_UID:uid},
        document, upsert=upsert, multi=False, omit_payload=omit_payload,
        atomic=atomic)


#@rest
#def patch_document(db, collection, uid):
#    """
#    Patch a document
#
#    """
#    document = request.json
#    if not document:
#        raise RESTError("document not provided", status=400)
#
#    omit_payload = to_bool(request.params.get("omit_payload", ""))
#
#    store = prepare_store(db)
#
#    return store.patch_document(uid, document, omit_payload=omit_payload)


@rest
def delete_document(db, collection, uid):
    """
    Delete a document

    """
    store = prepare_store(db)

    store._delete(uid)

    if store.retrieve(uid):
        raise RESTError("document could not be deleted")

    return {"success":True}


@rest
def get_document_hierarchy(db, collection, uid):
    """
    Get a document hierarchy / tree

    """
    store = prepare_store(db)
    
    doc = store.retrieve(uid)
    if not doc:
        raise RESTError("document `{0}` not found".format(uid), status=404)

    root, tree = store.retrieve_hierarchy(uid)

    def _convert(node_list):
        return [str(b) for a, b in node_list]

    tree = dict((str(key), _convert(value)) for key, value in tree.items())

    return {"success":True, "root":root, "tree":tree}


@rest
def get_document_ancestors(db, collection, uid):
    """
    Get a document ancestor list

    """
    store = prepare_store(db)

    doc = store.retrieve(uid)
    if not doc:
        raise RESTError("document `{0}` not found".format(uid), status=404)

    root, ancestors = store.retrieve_ancestors(uid)

    return {"success":True, "root":root, "ancestors":[str(node) for node in ancestors]}


def check_collection(store, collection):
    if not store.collection_exists(collection):
        raise RESTError("collection `{0}` does not exist".format(collection), status=404)


@rest
def get_collection_indexes(db, collection):
    """ 
    Get the index definitions for a collection

    """
    store = prepare_store(db)

    check_collection(store, collection)

    indexes = store.collection_indexes(collection)
    if not indexes:
        raise RESTError("collection `{0}` has no indexes".format(collection), status=404)

    return dict((k, v.to_dict()) for k,v in indexes.items())


@rest
def delete_collection_indexes(db, collection):
    """
    Delete the index definitions and data for a collection

    """
    store = prepare_store(db)
    
    check_collection(store, collection)
     
    store.collection_drop_indexes(collection)

    if store.collection_indexes(collection):
        raise RESTError("collection indexes could not be deleted")

    return {"success":True}


@rest
def reindex_collection_indexes(db, collection):
    """
    Reindex the documents in the given collection with the indexes in the 
    given collection.

    """
    store = prepare_store(db)

    check_collection(store, collection)

    if not store.collection_indexes(collection):
        raise RESTError("collection has no indexes to reindex", status=404)

    if not store.collection_reindex_indexes(collection):
        raise RESTError("collection reindexing failed")

    return {"success":True}


@rest
def get_indexes(db):
    """
    Get all index definitions

    """
    store = prepare_store(db)
    
    indexes = store.indexes()
    if not indexes:
        raise RESTError("database has no indexes", status=404)

    return dict((key,index.to_dict()) for key, index in indexes.items())


@rest
def get_index(db, name):
    """
    Get the index definitions and data for a specific index

    """
    store = prepare_store(db)
    
    index = store.index_retrieve(name)
    if not index:
        raise RESTError("index `{0}` does not exist".format(name), status=404)

    return index.to_dict()


@rest
def delete_index(db, name):
    """
    Delete the index definitions and data for a specific index

    """
    store = prepare_store(db)
    
    index = store.index_retrieve(name)
    if not index:
        raise RESTError("index `{0}` does not exist".format(name), status=404)

    store.index_drop(name)

    index = store.index_retrieve(name)
    if index:
        raise RESTError("index `{0}` could not be deleted".format(name))

    return {"success":True}


@rest
def ensure_collection_index(db, collection):
    """
    Ensure a collection is indexed

    def index_ensure(self, collection, fields, unique=False, sparse=False,
            reindex_all=False, key=None):
    """
    fields = to_array(request.query.get("fields", ""))
    if not fields:
        raise RESTError("missing required parameter `fields`", status=400)

    params = {
        "unique":      to_bool(request.query.get("unique", "")),
        "sparse":      to_bool(request.query.get("sparse", "")),
        "reindex_all": to_bool(request.query.get("reindex_all", "")),
        "key":         request.query.get("key", None)
    }

    kwargs = dict((key, value) for key, value in params.items() if value is not None)    

    store = prepare_store(db)
    
    check_collection(store, collection)

    index = store.index_ensure(collection, fields, **kwargs)
    if not index:
        raise RESTError("collection index could not be created")
    
    return index.to_dict()


def main():
    install(JSONAPIPlugin())

    for plugin in app[0].plugins:
        if isinstance(plugin, JSONPlugin):
            plugin.json_dumps = json_dumps
    prepare_routing()
    return run(host="0.0.0.0", port="8087", reloader=True)


if __name__ == "__main__":
    main()
