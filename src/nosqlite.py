# -*- coding: utf-8 -*-

"""
NoSQLite: A "nosql" document-oriented database implementation using
          json, bson, or pickle, and sqlite3.

Copyright (c) 2012, Jordan Sherer
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""

__version__      = "0.0"
__author__       = "Jordan Sherer"
__author_email__ = "jordan@widefido.com"
__license__      = "BSD2"
__url__          = "http://bitbucket.org/widefido/nosqlite"


import collections
import datetime
import json
import itertools
import functools
import logging
import os
import re
import sqlite3
import sys
import uuid
import zlib


# SQLite3 3.6.19 introduced proper foreign key support and is required
if tuple(int(x) for x in sqlite3.sqlite_version.split(".")) < (3,6,19):
    raise ImportError("SQLite version 3.6.19 or greater required")


# Check to see if bson is supported (Python 3.x or greater) and import
try:
    if sys.version_info < (3, 0):
        sys.stderr.write("NOTE: bson support disabled (python 2.x)\n")
        raise ImportError
    import bson
    BSON_AVAILABLE = True
except ImportError:
    BSON_AVAILABLE = False


# Check to see if pickle is supported and prepare a pickle loader with type
# restrictions (limited to the standard "safe" JSON data types)
try:
    import pickle
    PICKLE_AVAILABLE = True

    import builtins
    import io

    safe_builtins = {'None', 'int', 'float', 'bool', 'str', 'bytes', 'list',
        'tuple', 'dict'}

    class RestrictedUnpickler(pickle.Unpickler):
        def find_class(self, module, name):
            if module == "builtins" and name in safe_builtins:
                return getattr(builtins, name)
            raise pickle.UnpicklingError("global '%s.%s' is forbidden" %
                                        (module, name))

    def pickle_loads(s):
        """Helper function analogous to pickle.loads()."""
        return RestrictedUnpickler(io.BytesIO(s)).load()
except ImportError:
    PICKLE_AVAILABLE = False


__all__ = ["Database", "Storage"]


sqlite3.enable_callback_tracebacks(True)

log = logging.getLogger("nosqlite")
log.addHandler(logging.StreamHandler(stream=sys.stderr))
log.setLevel(logging.ERROR)
#log.setLevel(logging.INFO)


class StorageError(Exception):
    """Base exception for errors in the storage class"""
    pass


class DatabaseError(StorageError):
    """Error in the database"""
    pass


class IndexError(DatabaseError):
    """Error when an index contraint cannot be met in the storage class"""
    pass


class QueryError(DatabaseError):
    """Error when a query cannot be evaluated (syntaxual or contextual)"""
    pass


class DatabaseContextManger(object):
    """
    A class that allows for a pseudo-nested sqlite connection

    """
    def __init__(self, connection):
        self.connection = connection
        self.depth = 0
        self.value = None
    

    def __enter__(self):
        """
        Enter a context, if not already in one.
        Otherwise, just increase the depth of the current context.

        """
        if self.depth == 0:
            self.depth = 1
            self.value = self.connection.__enter__()
            return self.value

        self.depth += 1
        return self.value
    

    def __exit__(self, exc_type, exc_value, traceback):
        """
        Exit a context. If an exception was thrown, rollback.
        Otherwise, just decrease the depth of the current context.

        """
        self.depth -= 1
        
        if self.depth == 0 or exc_type is not None:
            return self.connection.__exit__(exc_type, exc_value, traceback)


class Database(object):
    """
    Wrapper around a SQLite3 connection, allowing for nested contexts,
    tracing, and other helper functionality.
    
    """
    def __init__(self, path, autoconnect=True, on_new=None, trace=False):
        self.path = path
        self.connection = None
        self.is_new = False
        self.on_new = on_new
        self.context = None
        self.trace = trace

        if trace:
            log.setLevel(logging.INFO)

        if autoconnect:
            self.connect()
    

    def __enter__(self):
        """
        Use the database context wrapper for allowing the database
        to be used with the with operator:

        >>> d = Database()
        >>> with d:
        ...     d.execute(...)   

        """
        if not self.context:
            raise DatabaseError("Cannot enter an undefined context")

        return self.context.__enter__() 
    

    def __exit__(self, exc_type, exc_value, traceback):
        """
        Use the database context wrapper for allowing the database
        to be used with the with operator:

        >>> d = Database()
        >>> with d:
        ...     d.execute(...)   

        """
        if not self.context:
            raise DatabaseError("Cannot exit from an undefined context")

        return self.context.__exit__(exc_type, exc_value, traceback)
    
    
    def connect(self):
        """
        Connect to the sqlite database provided in path in the constructor

        """
        if not os.path.exists(self.path):
            self.is_new = True
        
        # register adapter for UID datatype
        sqlite3.register_adapter(uuid.UUID, adapt_uid)
        sqlite3.register_converter("UID", convert_uid)
        
        # create our connection object
        self.connection = sqlite3.connect(self.path,
            detect_types=sqlite3.PARSE_DECLTYPES | sqlite3.PARSE_COLNAMES)

        # enable foreign keys
        self.connection.execute("PRAGMA foreign_keys = ON")

        # define our custom functions
        self.connection.create_function("payload_field", 3, payload_field)
        self.connection.create_function("payload_field", 4, payload_field)
        self.connection.create_function("regexp", 2, regexp)

        # use the sqlite3.Row class for row objects
        self.connection.row_factory = sqlite3.Row
        
        # create a new database context manager
        self.context = DatabaseContextManger(self.connection)

        if self.is_new and callable(self.on_new):
            self.on_new(self)


    def close(self):
        """
        Close the connection.

        """
        if self.connection:
            self.connection.close()
        


    def commit(self, *args, **kwargs):
        """
        Proxy to the connection.commit function

        """
        return self.connection.commit(*args, **kwargs)


    def execute(self, *args, **kwargs):
        """
        Proxy to the connection.execute function

        """
        if self.trace:
            log.info(args)
        return self.connection.execute(*args, **kwargs)
    

    def executemany(self, *args, **kwargs):
        """
        Proxy to the connection.executemany function

        """
        return self.connection.executemany(*args, **kwargs)
    
    
    def executescript(self, *args, **kwargs):
        """
        Proxy to the connection.executescript function

        """
        return self.connection.executescript(*args, **kwargs)


    def table_exists(self, table):
        """
        Return whether or not the table exists

        """
        cursor = self.execute("SELECT 1 FROM sqlite_master WHERE type = ? AND name = ?", ("table", table))
        if not cursor:
        	return False
        results = cursor.fetchone()
        if not results:
            return False
        return True
    

    def table_info(self, table):
        """
        Return a dictionary object of table info or None if the table does
        not exist.

        The dictionary object has two keys:
            1) column_names: a list of ordered column names
            2) column_info: a dictionary of columnar information
        
        """
        cursor = self.execute("PRAGMA table_info('{0}')".format(table))
        results = cursor.fetchall()
        if not results:
            return None
        
        table_info = {"column_names":[], "column_info":{}}
        for result in results:
            table_info["column_names"].append(result[1])
            table_info["column_info"][result[1]] = {"type":result[2],
                "nullable":result[3],
                "default":result[4]}

        return table_info


class Index(object):
    """
    Serialize/unserialize and access index data

    """
    _COLLECTION = "c"
    _FIELDS = "f"
    _SPARSE = "s"
    _UNIQUE = "u"
    _KEY = "k"

    @staticmethod
    def from_payload(payload):
        d = Dot(unserialize(payload))
        
        collection = d.get(Index._COLLECTION, None)
        fields = d.get(Index._FIELDS, None)
        sparse = d.get(Index._SPARSE, None)
        unique = d.get(Index._UNIQUE, None)
        key = d.get(Index._KEY, None)
        
        if collection is None or fields is None or sparse is None or \
                unique is None or key is None:
            return None

        return Index(collection, fields, unique, sparse, key=key)

    
    def __init__(self, collection, fields, unique, sparse, key=None):
        if not key:
            key = _index_key(collection, fields, unique=unique, sparse=sparse)

        self.collection = collection
        self.fields = tuple(fields)
        self.unique = unique
        self.sparse = sparse
        self.key = key

    
    def to_dict(self):
        return { "collection": self.collection,
                 "fields": self.fields,
                 "sparse": self.sparse,
                 "unique": self.unique,
                 "key": self.key }


    def to_payload(self):
        return serialize({
            Index._COLLECTION: self.collection,
            Index._FIELDS: self.fields,
            Index._SPARSE: self.sparse,
            Index._UNIQUE: self.unique,
            Index._KEY: self.key })


# Document Key Constants
META_KEY_UID = "_uid"
META_KEY_PARENT_UID = "_parent_uid"
META_KEY_COLLECTION = "_collection"
META_KEY_VERSION = "_version"
META_KEY_CREATED = "_created"
META_KEY_UPDATED = "_updated"
META_KEY_PAYLOAD = "_payload"
META_KEYS = (META_KEY_UID,
    META_KEY_PARENT_UID,
    META_KEY_COLLECTION,
    META_KEY_VERSION,
    META_KEY_CREATED,
    META_KEY_UPDATED, 
    META_KEY_PAYLOAD)


class Storage(object):
    """
    Storage is the main nosql interface to storing documents.

    Documents are organized into groups of "collections" in the database.
    These groups can be indexed and queried, and allow for documents
    to be retrieved efficiently by their unique id (UUID4).

    Storage uses SQLite for its backend. Upon creating a new Storage object, 
    a SQLite database is either loaded or created. By specifying a path, the 
    storage object can load any SQLite database that Python can access, 
    including a database stored in memory (default). 

    """
    def __init__(self, path=":memory:", trace=False):
        self.database = Database(path, autoconnect=True, trace=trace,
            on_new=self._on_new)
    
    
    def _on_new(self, database):
        """
        Callback to create the required base tables for storage.
        
        """
        if not database:
            raise StorageError("Database not connected")
        
        with database:
            database.executescript("""
                CREATE TABLE IF NOT EXISTS collection (
                    name TEXT PRIMARY KEY );

                CREATE TABLE IF NOT EXISTS indexes (
                    name TEXT PRIMARY KEY,
                    collection TEXT,
                    payload BLOB,
                    FOREIGN KEY (collection) REFERENCES collection (name)
                        ON DELETE CASCADE);
                
                CREATE TABLE IF NOT EXISTS entity (
                    added_id INTEGER PRIMARY KEY AUTOINCREMENT,
                    collection TEXT,
                    uid UID UNIQUE,
                    parent_uid UID,
                    version INTEGER DEFAULT 1,
                    created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                    updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                    payload BLOB,
                    FOREIGN KEY (parent_uid) REFERENCES entity (uid),
                    FOREIGN KEY (collection) REFERENCES collection (name)
                        ON DELETE CASCADE);

                --CREATE TABLE IF NOT EXISTS entity_relationship (
                --    left_id UID, 
                --    right_id UID, 
                --    relation TEXT,
                --    FOREIGN KEY (left_id) REFERENCES entity (uid)
                --        ON DELETE CASCADE,
                --    FOREIGN KEY (right_id) REFERENCES entity (uid)
                --        ON DELETE CASCADE);
                
                CREATE TABLE IF NOT EXISTS entity_closure (
                    parent_id UID,
                    child_id UID,
                    depth INTEGER DEFAULT 0,
                    FOREIGN KEY (parent_id) REFERENCES entity(uid),
                    FOREIGN KEY (child_id) REFERENCES entity(uid));

                CREATE INDEX IF NOT EXISTS idx_indexes_collection
                    ON indexes (collection);
                CREATE INDEX IF NOT EXISTS idx_entity_uid ON entity (uid);
                CREATE INDEX IF NOT EXISTS idx_entity_collection
                    ON entity (collection);

                CREATE TRIGGER IF NOT EXISTS trigger_entity_version_increment
                    AFTER UPDATE ON entity FOR EACH ROW
                BEGIN
                    UPDATE entity SET version = version + 1 WHERE NEW.uid = uid;
                END;

                CREATE TRIGGER IF NOT EXISTS trigger_entity_insert
                    AFTER INSERT ON entity
                FOR EACH ROW BEGIN
                    -- insert the self node.
                    INSERT INTO entity_closure (parent_id, child_id, depth)
                        VALUES (new.uid, new.uid, 0);
                    
                    -- insert the parent links
                    INSERT INTO entity_closure (parent_id, child_id, depth)
                        SELECT p.parent_id, c.child_id, p.depth + c.depth + 1
                        FROM entity_closure p, entity_closure c
                        WHERE
                            p.child_id = new.parent_uid
                            AND c.parent_id = new.uid;
                END;

                CREATE TRIGGER IF NOT EXISTS trigger_entity_delete
                    BEFORE DELETE ON entity
                FOR EACH ROW BEGIN
                    -- delete node from tree along with the subtree
                    DELETE FROM entity_closure WHERE rowid IN (
                        SELECT DISTINCT link.rowid FROM entity_closure link
                            LEFT OUTER JOIN entity_closure p
                            LEFT OUTER JOIN entity_closure c
                            LEFT OUTER JOIN entity_closure to_delete
                        WHERE
                            p.parent_id = link.parent_id
                            AND c.child_id = link.child_id
                            AND p.child_id = to_delete.parent_id
                            AND c.parent_id = to_delete.child_id
                            AND (to_delete.parent_id = old.uid
                                 OR to_delete.child_id = old.uid)
                            AND to_delete.depth < 2);
                END;

                CREATE TRIGGER IF NOT EXISTS trigger_entity_update
                    AFTER UPDATE OF parent_uid ON entity
                FOR EACH ROW BEGIN
                    -- delete the ancestor links from the starting node id
                    DELETE FROM entity_closure WHERE
                        entity_closure.child_id IN (
                            SELECT child_id FROM entity_closure
                                WHERE parent_id = old.uid)
                        AND entity_closure.parent_id NOT IN (
                            SELECT child_id FROM entity_closure
                                WHERE parent_id = old.uid);

                    -- insert new ancestor links
                    INSERT INTO entity_closure (parent_id, child_id, depth)
                    SELECT p.parent_id, c.child_id, p.depth + c.depth + 1
                        FROM entity_closure p JOIN entity_closure c
                        WHERE c.parent_id = old.uid
                            AND p.child_id = new.parent_uid;
                END;
                """)
            
            assert(database.table_exists("collection"))
            assert(database.table_exists("indexes"))
            assert(database.table_exists("entity"))

            info = database.table_info("entity")
            assert(len(info["column_names"]) == 8)
   
    
    def __enter__(self):
        """
        Use the database context wrapper for allowing the database
        to be used with the with operator:

        >>> d = Database()
        >>> with d:
        ...     d.execute(...)   

        """
        if not self.database:
            raise StorageError("Cannot enter a context without a database")

        self.database.__enter__()

        return self
    

    def __exit__(self, exc_type, exc_value, traceback):
        """
        Use the database context wrapper for allowing the database
        to be used with the with operator:

        >>> d = Database()
        >>> with d:
        ...     d.execute(...)   

        """
        if not self.database:
            raise StorageError("Cannot exit from a context without a database")

        return self.database.__exit__(exc_type, exc_value, traceback)

    
    def collections(self):
        """
        Get a list of collections.

        """
        with self.database:
            results = self.database.execute("SELECT name FROM collection")
            if results:
                return [row["name"] for row in results.fetchall()]
        return []
    

    def collection_exists(self, collection):
        """
        Return whether or not a collection exists.
    
        """
        with self.database:
            results = self.database.execute("SELECT 1 FROM collection "
                "WHERE name = ?", (collection,))

            return not not (results and results.fetchone())
        return False


    def collection_create(self, collection):
        """
        Create a collection with the given payload.
        
        """
        if self.collection_exists(collection):
            return

        with self.database:
            self.database.execute("""INSERT INTO collection
                (name) VALUES (?)""", (collection,))
        
        assert(self.collection_exists(collection))

        return True


    def collection_drop(self, collection):
        """
        Drop a collection, its indices, and all entities

        """
        with self.database:
            self.collection_drop_indexes(collection)
            self.database.execute("DELETE FROM collection WHERE name = ?",
                (collection,))
        
        assert(not self.collection_exists(collection))

        return True

    
    def collection_indexes(self, collection, cleanup=False):
        """
        Return a dictionary of index objects. If `cleanup` is true, delete
        dangling index references.

        """
        with self.database:
            results = self.database.execute("SELECT name, collection, payload "
                "FROM indexes WHERE collection = ?", (collection,))

            if results:
                indexes = {}
                for row in results.fetchall():
                    if not self.index_exists(row["name"]):
                        if cleanup:
                            self.database.execute("DELETE FROM indexes WHERE "
                                "name = ?", (key,))
                        continue
                    indexes[row["name"]] = Index.from_payload(row["payload"])
                return indexes
    

    def collection_reindex_indexes(self, collection):
        """
        Reindex of all documents in the collection.
        
        """
        indexes = self.collection_indexes(collection)

        if not indexes:
            return False

        for doc in self.retrieve_all(collection):
            self.index_document(doc)

        return True


    def collection_drop_indexes(self, collection):
        """
        Drop the indexes for a collection

        """
        with self.database:
            indexes = []
            
            c = self.database.execute("SELECT name FROM indexes WHERE "
                "collection = ?", (collection,))

            for row in c.fetchall():
                indexes.append(row["name"])

            self.database.execute("DELETE FROM indexes WHERE collection = ?",
                (collection,))

            for index in indexes:
                self.index_drop(index)


    def indexes(self, cleanup=False):
        """
        Retrieve all index definitions in the database. If `cleanup` is true,
        delete dangling index references.

        """
        with self.database:
            results = self.database.execute("SELECT name, payload "
                "FROM indexes")

            if results:
                indexes = {}
                for index in results.fetchall():
                    if not self.index_exists(index["name"]):
                        if cleanup:
                            self.database.execute("DELETE FROM indexes WHERE "
                                "name = ?", (key,))
                        continue
                    indexes[index["name"]] = Index.from_payload(index["payload"])
                return indexes

    
    def index_exists(self, key):
        """
        Return whether or not an index table exists with the given key.

        """
        with self.database:
            return self.database.table_exists(key)

    
    def index_retrieve(self, key, cleanup=False):
        """
        Retrieve an index object from the index table with the given key.
        If `cleanup` is true, delete dangling index references.

        """
        with self.database:
            results = self.database.execute("SELECT name, payload "
                "FROM indexes WHERE name = ? LIMIT 1", (key,))

            if not results:
                return

            index = results.fetchone()
            if not index:
                return

            if cleanup and not self.index_exists(key):
                self.index_drop(key)
                return

            return Index.from_payload(index["payload"])


    def index_drop(self, key):
        """
        Drop an index with the given key.

        """
        with self.database:
            self.database.execute("DELETE FROM indexes WHERE name=?", (key,))
            self.database.execute("DROP TABLE IF EXISTS {0}".format(escape(key)))
    
    
    def index_drop_all(self):
        """
        Drop all indexes.

        """
        indexes = self.indexes()
        for index in indexes:
            self.index_drop(index)


    def index_create(self, collection, fields, unique=False, sparse=False,
            key=None):
        """
        Create an index with the given key.

        """
        if not self.collection_exists(collection):
            self.collection_create(collection)
       
        if not key:
            key = _index_key(collection, fields, unique=unique, sparse=sparse)

        col = []

        col.append("uid UID")

        for field in fields:
            col.append("{0} BLOB".format(escape(field)))

        col.append("FOREIGN KEY (uid) REFERENCES entity (uid) "
                   "ON DELETE CASCADE")
        
        if unique:
            col.append("UNIQUE({0})".format(
                ", ".join([escape(field) for field in fields])))

        # create the index table
        query = "CREATE TABLE {0} ({1})".format(escape(key), ", ".join(col))
        self.database.execute(query)

        assert(self.database.table_exists(key))

        # create an index on the uid column of the index table
        query = "CREATE INDEX {0} ON {1} (uid)".format(
            escape("idx_{0}_uid".format(key)),
            escape(key))
        self.database.execute(query)

        # Create the index object for storage
        index = Index(collection, fields, unique, sparse, key=key)

        # Insert an entry into the index table
        query = ("INSERT INTO indexes (name, collection, payload)"
                 "VALUES (?,?,?)")
        self.database.execute(query, (key, collection, index.to_payload()))

        self.database.commit()
        
        return index

    
    def index_ensure(self, collection, fields, unique=False, sparse=False,
            reindex_all=False, key=None):
        """
        Ensure that the collection has this index defined, then
        start indexing on all documents inserted after this statement.
        
        """
        if not isinstance(fields, list) and not isinstance(fields, tuple):
            fields = (fields,)
        
        if key is None:
            key = _index_key(collection, fields, unique=unique, sparse=sparse)

        if self.index_exists(key):
            index = self.index_retrieve(key)
        else:
            with self.database:
                index = self.index_create(collection, fields, unique=unique, sparse=sparse,
                    key=key)
                assert(self.index_exists(key))
        
        if reindex_all:
            self.collection_reindex_indexes(collection)

        return index
    

    def index_document(self, doc):
        """
        Reindex the given document in the collection. If the document
        does not exist in the collection (either it does not have a
        uid that exists or does not have a collection key) then a
        ValueError is raised.

        """
        if META_KEY_UID not in doc or META_KEY_COLLECTION not in doc:
            raise IndexError("Document must be inserted into collection "
                "before indexed")

        collection = doc[META_KEY_COLLECTION]

        indexes = self.collection_indexes(collection)
        if not indexes:
            return False
        
        try:
            for index_key in indexes:
                self.index_document_with_index(doc, index_key,
                    indexes[index_key])
            return True
        except Exception as e:
            raise IndexError("Failed to index document: %s" % e)
        
    
    def index_document_with_index(self, doc, index_key, index):
        """
        Index a document with the given index criteria. `index` is a
        dictionary of indexing specifications returned from index_create or 
        index_retrieve.

        """
        if not doc or not index:
            raise IndexError("Cannot index doc {0} in index {1}".format(
                doc, index_key))

        document = Dot(doc)

        values = collections.OrderedDict()
        multi = set()

        for field in index.fields:
            exists = field in document

            if index.sparse and not exists:
                continue
            
            if exists:
                value = document[field]
            else:
            	value = None

            if isinstance(value, list) or isinstance(value, tuple):
                multi.add(field)

            values[field] = value

        if not values and not multi:
            return

        if len(multi) > 1:
            # only a single multi-field can be indexed at a time
            # this is because the resulting index would be a cartesian
            # product of each field. This is exponential and can easily get
            # out of hand. For example, two, 2-element lists, we would have
            # to store 4 index rows: A * B = (A1 B1) (A1 B2) (A2 B1) (A2 B2), 
            # and for two, 4-element lists, we would store 16 rows, and so on.
            raise IndexError("Multi-indexes can only support indexing a "
                "single list field per index.")

        uid = document[META_KEY_UID]

        values["uid"] = uid

        with self.database:
            delete_q = "DELETE FROM {0} WHERE uid = ?".format(escape(index_key))
            self.database.execute(delete_q, (uid,))

            insert_q = "INSERT INTO {0} ({1}) VALUES ({2})".format(
                escape(index_key),
                ", ".join((escape(key) for key in values.keys())),
                ", ".join(["?"] * len(values.keys())))

            if multi:
                field = multi.pop()
                for value in document[field]:
                    values[field] = value
                    self.database.execute(insert_q, list(values.values()))
            else:
                self.database.execute(insert_q, list(values.values()))
   
    
    def exists(self, uid):
        """
        Return whether or not a document with a given uid exists.

        """
        with self.database:
            results = self.database.execute("SELECT 1 FROM entity "
                "WHERE uid = ?", (uid,))

            return results and results.fetchone()

    
    def retrieve(self, uid, fields=None):
        """
        Retrieve a doc in a given collection by its uid.

        """
        with self.database:
            cursor = self.database.execute("SELECT uid, parent_uid, collection, version, "
                "created, updated, payload FROM entity WHERE uid = ?", (uid,))

            if not cursor:
                return

            row = cursor.fetchone()
            
            transformer = _transform_doc_fields(fields)
            return _row_to_doc(row, transformer=transformer)


    def retrieve_hierarchy(self, uid):
        """
        Retrieve a doc hierarchy (decendents) rooted at uid.
        Returns a dictionary (which acts as a tree, keys are nodes,
        values are lists of children). The tree returned includes the root.

        """
        with self.database:
            cursor = self.database.execute("SELECT parent_id, entity.parent_uid as parent_uid, child_id, depth "
                "FROM entity_closure "
                "LEFT JOIN entity ON entity.uid = child_id "
                "WHERE parent_id = ? AND entity.parent_uid IS NOT NULL "
                "ORDER BY entity_closure.depth ASC, entity_closure.rowid", (uid,))
            
            if not cursor:
                return

            results = [Node(row["parent_uid"], row["child_id"]) for row in cursor.fetchall()]

            tree = groupby(results, lambda x: x[0])
            
            return uid, tree


    def retrieve_ancestors(self, uid):
        """
        Retrieve a doc hierarchy (ancestors) rooted at uid.
        Returns a list which includes the root.

        """
        with self.database:
            cursor = self.database.execute("SELECT entity.parent_uid as parent_uid, entity.uid as child_id, depth "
                "FROM entity_closure "
                "LEFT JOIN entity ON entity.uid = parent_id "
                "WHERE child_id = ? "
                "ORDER BY entity_closure.depth ASC", (uid,))
            
            if not cursor:
                return

            return uid, [Node(row["parent_uid"], row["child_id"]) for row in cursor.fetchall()]


    def retrieve_all(self, collection, fields=None):
        """
        Retrieve all docs in a given collection.

        """
        generator = self.retrieve_all_iter(collection, fields=fields)

        if not generator:
            return None

        return list(generator)
    

    def retrieve_all_iter(self, collection, fields=None):
        """
        Retrieve all docs in a given collection.

        """
        query = """SELECT uid, parent_uid, collection, created, version, updated, payload
            FROM entity WHERE collection = ?"""
        values = (collection,)
        return self.retrieve_by_sql_iter(query, values, fields=fields)

   
    def retrieve_by_sql(self, query, values=None, fields=None):
        """
        Retrieve docs by a SQL query

        """
        generator = self.retrieve_by_sql_iter(query, values=values,
            fields=fields)
        
        if not generator:
            return None

        return list(generator)


    def retrieve_by_sql_iter(self, query, values=None, fields=None):
        """
        Retrieve docs by a SQL query

        """
        with self.database:
            if values is not None:
                cursor = self.database.execute(query, values)
            else:
                cursor = self.database.execute(query)
            
            if not cursor:
                return

            transformer = _transform_doc_fields(fields)            

            return (_row_to_doc(row, transformer=transformer) for row in cursor)


    def find(self, collection, query=None, fields=None, sort=None, skip=None,
            limit=None, use_natural=True):
        """
        Find documents in the passed collection, filtering based on
        `query`, skipping the first `skip` documents, limited to `limit`
        number of documents returned. If the documents in the collection
        do not have indexes that match the fields in `query`, find will
        return all of the documents, unless `use_natural` is true, in
        which case, the find will do an in-memory comparison of the docs. 

        """
        generator = self.find_iter(collection,
            query=query,
            fields=fields,
            sort=sort,
            skip=skip,
            limit=limit,
            use_natural=use_natural)
        
        if not generator:
            return None

        return list(generator)


    def find_one(self, collection, query=None, fields=None, sort=None,
            skip=None, use_natural=True):
        """
        Convenience function to return only the first result of the 
        find function directly (i.e., not in a list with a length of 1)

        """
        items = self.find(collection, query=query, fields=fields, sort=sort,
            skip=skip, limit=1, use_natural=use_natural)

        if not items:
            return None

        return items[0]

    
    def find_iter(self, collection, query=None, fields=None, sort=None,
            skip=None, limit=None, use_natural=True):
        """
        Find documents in the passed collection, filtering based on `query`,
        skipping the first `skip` documents, limited to `limit` documents
        returned. If the documents in the collection do not have indexes that 
        match the fields in `query`, find will return all of the documents, 
        unless `use_natural` is true, in which case, the find will do an 
        in-memory comparison of the docs. 

        """
        sql, values = self._generate_find_sql(collection,
            query=query,
            sort=sort,
            skip=skip,
            limit=limit,
            use_natural=use_natural)
    
        return self.retrieve_by_sql_iter(sql, values=values, fields=fields)


    def find_count(self, collection, query=None, fields=None, sort=None,
            skip=None, limit=None, use_natural=True):
        """
        Count the number of matching documents that would be returned if 
        the same parameters were passed into the find method.

        """
        sql, values = self._generate_find_sql(collection,
            query=query,
            sort=sort,
            skip=skip,
            limit=limit,
            use_natural=use_natural)

        return self._count_find_sql(sql, values=values)

    
    def _count_find_sql(self, sql, values=None):
        """
        Wrap a sql statement in a generic count query and return the number
        of rows that would be returned upon querying the database with the 
        provided sql.

        """
        count_sql = "SELECT count(*) as count FROM ({0}) AS q".format(sql)

        if values:
            c = self.database.execute(count_sql, values)
        else:
            c = self.database.execute(count_sql)
        
        if not c:
            return 0

        return c.fetchone()["count"]


    def _generate_find_sql(self, collection, query=None, sort=None, skip=None,
            limit=None, use_natural=True):
        """
        Find documents in the passed collection, filtering based on `query`,
        skipping the first `skip` documents, limited to `limit` documents
        returned. If the documents in the collection do not have indexes that 
        match the fields in `query`, find will return all of the documents, 
        unless `use_natural` is true, in which case, the find will do an 
        in-memory comparison of the docs. 

        """
        meta_fields_set = set(META_KEYS)

        indexes = self.collection_indexes(collection)
        
        field_aliases = {}
        index_enum = {}
        column_enum = {}
        indexed_fields_set = set()
        indexed_fields_map = {}

        if indexes:
            for index in indexes.values():
                indexed_fields_set.update(index.fields)

                for field in index.fields:
                    if field not in indexed_fields_map:
                        indexed_fields_map[field] = [index]
                    else:
                        indexed_fields_map[field].append(index)
        

        q = SQLSelect()
        q.columns("e.uid",
            "e.parent_uid",
            "e.collection",
            "e.version",
            "e.created",
            "e.updated",
            "e.payload")

        q.from_("entity", "e")

        if collection:
            q.where("e.collection = ?", [collection])

        
        def add_index_for_field(f):
            indexes = indexed_fields_map[f]
            index = indexes[0]

            if index.key not in index_enum:
                index_enum[index.key] = len(index_enum)
                index_type = "INNER" if not index.sparse else "LEFT"
                join = "{0} JOIN {1} i{2} ON e.uid = i{2}.uid".format(
                    index_type, escape(index.key), index_enum[index.key])
                q.join(join)

            field_aliases[f] = "i{0}.{1}".format(index_enum[index.key],
                escape(f))
        

        def add_payload_for_field(f):
            if f not in column_enum:
                column_enum[f] = len(column_enum)
                q.columns("payload_field(e.rowid, e.payload, {0}, NULL) "
                    "as n{1}".format(escape(f), column_enum[f]))

            field_aliases[f] = "n{0}".format(column_enum[f])
        

        if query:
            if isinstance(query, str):
                query = ordered_json_loads(query)
            
            if not isinstance(query, dict):
                raise QueryError("Query must be a dictionary")

            query_fields = set(fields_from_query(query))

            natural_fields = query_fields - indexed_fields_set - meta_fields_set

            if not use_natural and len(natural_fields) > 0:
                log.info("cannot query natural field without use_natural=True")
                return "", []

            for i, f in enumerate(query_fields):
                if f in meta_fields_set:
                    field_aliases[f] = "e.{0}".format(f.strip("_"))

                elif f in indexed_fields_set:
                    add_index_for_field(f)

                elif use_natural:
                    add_payload_for_field(f)

                else:
                    raise QueryError("cannot query natural field `{0}` without use_natural=True".format(f))

            sql, vals = WhereClause(query).sql(field_aliases=field_aliases)
            q.where(sql, values=vals)


        if not sort: #sort is None:
            q.order_by("e.added_id")
        else:
            if isinstance(sort, str):
                sort = ordered_json_loads(sort)
            
            if not isinstance(sort, dict):
                raise QueryError("Sort expression must be a dictionary")

            sort = ((f, "DESC" if d < 0 else "ASC") for f, d in sort.items())

            for expr in sort:
                if len(expr) == 2:
                    f, d = expr
                elif len(expr) == 1:
                    f = expr[0]
                    d = "ASC"
                else:
                    raise QueryError("Invalid sort expression")

                if f not in field_aliases:
                    """
                    we could be requesting a sort on a field that isn't 
                    returned in the query, so we need to:
                    1) see if the field has an index and if so, add the index
                    2) otherwise, check to see if use_natural=True and if so
                       add in the payload field as a column for sorting
                    """
                    if f in indexed_fields_set:
                        add_index_for_field(f)

                    elif use_natural:
                        add_payload_for_field(f)

                    else:
                        raise QueryError("Sort expression must include an " 
                            "indexed field, or use_natural must be True")
                
                if f in field_aliases:
                    f = field_aliases[f]
                    q.order_by("{0} {1}".format(f, d))


        if limit is not None:
            q.limit("?", limit)

        if skip is not None:
            q.offset("?", skip)

        return q.generate()
    
    
    def insert(self, collection, doc, omit_payload=False):
        """ 
        Insert a doc into the collection. If doc has an id attribute, 
        do a save instead (basically an update based on _uid).

        """
        if not doc:
            raise StorageError("document `{0}` cannot be inserted".format(doc))

        if META_KEY_UID in doc:
            #return self.save(collection, doc, omit_payload=omit_payload)
            raise StorageError("document already inserted into collection")

        self.collection_create(collection)
        
        uid = create_uid()
        created = updated = datetime_now()

        if not omit_payload:
            payload = _doc_to_payload(doc)
        else:
            payload = None
        
        if META_KEY_PARENT_UID in doc:
            parent_uid = doc[META_KEY_PARENT_UID]
        else:
            parent_uid = None
        
        with self.database:
            self.database.execute("""INSERT INTO entity
                (uid, parent_uid, collection, payload) VALUES (?, ?, ?, ?)""",
                (uid, parent_uid, collection, payload))

            meta = { META_KEY_UID:uid, 
                META_KEY_PARENT_UID:parent_uid, 
                META_KEY_COLLECTION:collection, 
                META_KEY_VERSION:1,
                META_KEY_CREATED:created, 
                META_KEY_UPDATED:updated }
        
            doc.update(meta)
        
            self.index_document(doc)
        
        return doc


    def update(self, collection, query, doc, upsert=False, multi=False,
            sort=None, skip=None, omit_payload=False, atomic=False):
        """
        Update a series of documents in a collection based on the specified 
        `query`, a query that is used to find the documents to update. But,
        only the first document is updated, unless `multi` is True. If no 
        documents are found to be matching, the update performs no action, 
        unless `upsert` is True. In this instance, the document will be
        inserted into the collection. 

        Doc can be a document to insert in the place of the previous
        documents, or doc can be a callable function used to convert the
        stored document into the new document to be stored.

        If Doc is a set of key value pairs that are not atomic update operations
        the doc is replaced completely. Otherwise, the doc fields are incrementally 
        updated with the operations provided by doc.

        If atomic is True, the update function will update based on the version 
        identifier of the document. If the version identifiers do not match,
        no update will be performed and a StorageError will be thrown.
        
        The function uses this signature:
        >>> def converter(stored_document=None):
        >>>     ...
        >>>     return new_document

        """
        iterator = self.find_iter(collection,
                query=query,
                sort=sort,
                skip=skip,
                limit=1 if not multi else None,
                use_natural=True)

        if callable(doc):
            converter = doc
        else:
            import update
            converter = lambda s: update.update_document(s, doc)

        if iterator:
            documents = []
            for stored_doc in iterator:
                updated_doc = self._update(collection,
                    stored_doc,
                    converter(stored_doc),
                    omit_payload=omit_payload,
                    atomic=atomic)
                documents.append(updated_doc)
            if documents:
                return documents if multi else\
                    documents[0] if documents else None

        if upsert:
            d = converter(None)
            if META_KEY_UID in d:
                del d[META_KEY_UID]
            return self.insert(collection, d,
                omit_payload=omit_payload)


    def _update(self, collection, stored_doc, doc, omit_payload=False, atomic=False):
        """
        Update an object in the collection, updating indices as well. 

        Updates are performed by replacing the contents of stored_doc with
        the contents of doc. We do this by basically updating the storage
        location where stored_doc lives with the contents of doc.

        """
        if META_KEY_COLLECTION not in stored_doc:
            raise KeyError("doc must be registered in collection to update")

        if (META_KEY_COLLECTION in stored_doc and 
            collection is not None and
            stored_doc[META_KEY_COLLECTION] != collection):
            raise KeyError("doc must be registered in the collection provided")
        
        if META_KEY_UID not in stored_doc:
            raise KeyError("doc must already be inserted to update")

        version = doc[META_KEY_VERSION]
        uid = doc[META_KEY_UID] = stored_doc[META_KEY_UID]
        doc[META_KEY_COLLECTION] = stored_doc[META_KEY_COLLECTION]

        if collection:
            self.collection_create(collection)

        if not omit_payload:
            payload = _doc_to_payload(doc)
        else:
            payload = None

        if META_KEY_PARENT_UID in stored_doc:
            parent_uid = doc[META_KEY_PARENT_UID] = stored_doc[META_KEY_PARENT_UID]
        else:
            parent_uid = None
        
        with self.database:
            if atomic:
                results = self.database.execute("UPDATE entity SET "
                    "parent_uid = ?, updated = CURRENT_TIMESTAMP, payload = ? WHERE "
                    "uid = ? AND version = ?",
                    (parent_uid, payload, uid, version))
            else:
                results = self.database.execute("UPDATE entity SET "
                    "parent_uid = ?, updated = CURRENT_TIMESTAMP, payload = ? WHERE "
                    "uid = ?",
                    (parent_uid, payload, uid))

            if not results or results.rowcount <= 0:
                raise StorageError("document could not be updated")
        
            self.index_document(doc)

            return self.retrieve(uid) 

    
    #def patch(self, collection, query, updates, multi=False, sort=None,
    #        skip=None, omit_payload=False):
    #    """
    #    Patch an object in the collection.
    #    
    #    Patching is similar to an update. In fact, patch leverages an
    #    update with custom doc transformer. Patching allows for an easy
    #    way to update a certain subset of fields in a document, without
    #    having iterate over the documents manually. 

    #    Like an update, patch only patches the first item that matches the 
    #    query, unless multi=True. Unlike update, patch does not allow for an 
    #    upsert into the database. The query must match at least 1 item for
    #    the patch to succeed.

    #    >>> store = nosqlite.Storage()
    #    >>> store.insert("test", {"a":123, "new":False})
    #    >>> store.patch("test", {"a":123}, {"new":True})
    #    {"a":123, "new":True}

    #    """
    #    import update

    #    def _f(doc):
    #        return update.update_document(doc, updates)

    #    return self.update(collection, query,
    #        #lambda doc: _patch_doc(dict(doc), updates), multi=multi, sort=sort,
    #        _f, multi=multi, sort=sort,
    #        skip=skip, omit_payload=omit_payload)
   

    def delete(self, collection, query, skip=None, limit=None):
        """
        Delete a series of documents from the provided collection, matching
        the provided query. 

        """
        iterator = self.find_iter(collection, query=query, fields="_uid", \
                sort=None, skip=skip, limit=limit, use_natural=True)

        if iterator:
            uids = []
            with self.database:
                for doc in iterator:
                    uids.append(doc["_uid"])
                    self._delete(doc["_uid"])
            return uids


    def _delete(self, uid):
        """
        Perform a delete of a document with the provided uid. We rely on 
        foreign key cascading to delete the index entries as well.

        """
        with self.database:
            #indexes = self.collection_indexes(collection)
            #if indexes:
            #    for index in indexes.values():
            #        self.database.execute("DELETE FROM {0} "
            #            "WHERE uid = ?".format(escape(index.key)), (uid,))
            self.database.execute("DELETE FROM entity WHERE uid = ?", (uid,))


    def save_document(self, doc, omit_payload=False, atomic=True):
        """
        Save a document. Replaces the document completely if the document
        already exists.

        Equivalent to:
        >>> self.update(collection, {"_uid":doc["_uid"]}, doc, True, False)

        """
        if META_KEY_COLLECTION not in doc:
            raise StorageError("document must have a collection to be saved")

        if META_KEY_UID not in doc:
            raise StorageError("document must be inserted to save")
            #return self.insert(collection, doc, omit_payload=omit_payload)

        collection = doc[META_KEY_COLLECTION]
        uid = doc[META_KEY_UID]
        ver = doc[META_KEY_VERSION]
        
        return self.update(collection,
            {META_KEY_UID:uid, META_KEY_VERSION:ver} if atomic else {META_KEY_UID:uid},
            doc,
            upsert=True,
            multi=False,
            omit_payload=omit_payload,
            atomic=atomic)


    def update_document(self, doc, updates, omit_payload=False, atomic=True): 
        """
        Update a document with key-value pairs from the patch parameter.

        """
        if not doc:
            raise ValueError("doc must not be None")

        if META_KEY_UID not in doc:
            raise KeyError("doc must be inserted to patch")

        collection = doc[META_KEY_COLLECTION]
        uid = doc[META_KEY_UID]
        ver = doc[META_KEY_VERSION]

        return self.update(collection,
            {META_KEY_UID:uid, META_KEY_VERSION:ver} if atomic else {META_KEY_UID:uid},
            updates,
            upsert=False,
            multi=False,
            omit_payload=omit_payload,
            atomic=atomic)

    
    def delete_document(self, doc):
        """
        Delete a doc in the collection, updating indices as well.

        """
        if not doc:
            raise ValueError("doc must not be None")

        if META_KEY_UID not in doc:
            raise KeyError("doc must have a uid to delete")

        uid = doc[META_KEY_UID]
        
        self._delete(uid)

        return doc


def fields_from_query(query):
    """
    Return all the fields used in a query. All fields are on the top level,
    except ones nested in $and and $or clauses.
    
    """
    keys = []
    for key in query:
        if key in ("$and", "$or"):
            for v in query[key]:
                keys.extend(fields_from_query(v))
        else:
            keys.append(key)
    return keys


def items_from_query(query):
    """
    Return all the values used in a query. All values are on the top level,
    except ones nested in $and and $or clauses, and ones that are nested in 
    value dictionaries.
    
    """
    values = []
    for key, value in query.items():
        if key in ("$and", "$or"):
            for v in value:
                values.extend(items_from_query(v))
        else:
            if isinstance(value, dict):
                for k, v in value.items():
                    values.append((key, v))
            else:
                values.append((key, value))
    return values


class WhereClause(object):
    """
    A simple MongoDB-esque query parser to generate a SQL WHERE clause from
    a dictionary of terms. Also, provides an interface to use field aliases when 
    generating the SQL.

    """
    operations = {
        "$eq":"=",
        "$gt":">",
        "$lt":"<", 
        "$gte":">=",
        "$lte":"<=",
        "$ne":"!=",
        "$regex":"REGEXP",
        "$glob":"GLOB",
        "$match":"MATCH",
        "$like":"LIKE",

        "$in":"in",
        "$nin":"nin"
    }


    def __init__(self, query=None):
        self.query = query
        self.clauses = []
        self.binds = []
        self.keys = []
        self.field_aliases = {}
        self.count = 0

    
    def clause(self, key, value, operation=None):
        clauses = []
        binds = []
        keys = []

        if operation is None:
            operation = WhereClause.operations["$eq"]
        
        if key in ("$or", "$and"):
            if not isinstance(value, list) and not isinstance(value, tuple):
                raise ValueError("value must be a list or tuple when used with $or")

            group_clauses = []
            for v in value:
                subgroup_clauses = []
                for k in v:
                    c, b, ks = self.clause(k, v[k])
                    subgroup_clauses.extend(c)
                    binds.extend(b)
                    keys.extend(ks)
                group_clauses.append("({0})".format(" AND ".join(subgroup_clauses)))
            clauses.append("({0})".format(" {0} ".format("OR" if key == "$or" else "AND").join(group_clauses)))
                
        elif isinstance(value, dict):
            for operation in value:
                c, b, ks = self.clause(key, value[operation], WhereClause.operations[operation])
                clauses.extend(c)
                binds.extend(b)
                keys.extend(ks)

        else:
            keys.append(key)

            if operation in ("in", "nin"):
                if not isinstance(value, list) and not isinstance(value, tuple):
                    raise ValueError("value must be a list or tuple to be used with 'in' query operation")
                sub = ", ".join(["${0}".format(i + self.count) for i, v in enumerate(value)])
                clauses.append("{0} {1} IN ({2})".format(self.format_key(key), "NOT" if operation == "nin" else "", sub))
                binds.extend(value)
                self.count += len(value)
            else:
                clauses.append("{0} {1} ${2}".format(self.format_key(key), operation, self.count))
                binds.append(value)
                self.count += 1

        return clauses, binds, keys

    
    def format_key(self, key):
        """
        Return a field alias for key if exists, otherwise return the key as is.

        """
        if not self.field_aliases:
            return key
        return self.field_aliases.get(key, key)
    

    def sql(self, field_aliases=None):
        """ 
        Returns a generated SQL string and a list of binded values from the 
        query provided in the constructor.

        """
        self.field_aliases = field_aliases

        for key in self.query:
            clauses, binds, keys = self.clause(key, self.query[key])
            self.clauses.extend(clauses)
            self.binds.extend(binds)
            self.keys.extend(keys)

        if self.clauses:
            return " AND ".join(self.clauses), self.binds


class SQLSelect(object):
    """
    A simple (incomplete) helper class to build out a sql select statements
    
    """
    def __init__(self):
        self.reset()

    def reset(self):
        self._columns = []
        self._table = None
        self._joins = []
        self._where_clauses = []
        self._where_clause_values = []
        self._order_by = []
        self._order_by_values = []
        self._limit = []
        self._limit_values = []
        self._offset = []
        self._offset_values = []
    
    def columns(self, *columns):
        self._columns.extend(columns)
        return self

    def from_(self, name, alias=""):
        self._table = (name, alias)
        return self

    def join(self, expression):
        self._joins.append(expression)
        return self

    def where(self, clause, values=None):
        self._where_clauses.append(clause)
        if values:
            self._where_clause_values.extend(values)

    def order_by(self, expression, value=None):
        self._order_by.append(expression)
        if value is not None:
            self._order_by_values.append(value)
        return self

    def limit(self, expression, value=None):
        self._limit.append(expression)
        if value is not None:
            self._limit_values.append(value)
        return self
    
    def offset(self, expression, value=None):
        self._offset.append(expression)
        if value is not None:
            self._offset_values.append(value)
        return self
   
    def generate(self, field_aliases=None):
        sql = ["SELECT", ",".join(self._columns), "FROM",
               " ".join(self._table)]
        vals = []

        if self._joins:
            sql.extend(self._joins)
       
        if self._where_clauses:
            sql.append("WHERE")
            sql.append(" AND ".join(self._where_clauses))
            vals.extend(self._where_clause_values)
        
        if self._order_by:
            sql.append("ORDER BY")
            sql.append(",".join(self._order_by))
            vals.extend(self._order_by_values)

        if self._limit:
            sql.append("LIMIT")
            sql.append(",".join(self._limit))
            vals.extend(self._limit_values)

        if self._offset:
            sql.append("OFFSET")
            sql.append(",".join(self._offset))
            vals.extend(self._offset_values)

        return (" ".join(sql), vals) 


def _doc_to_payload(doc):
    """
    Delete items stored in the same row:
    1. _uid, _parent_uid, _collection, _created, _updated
    
    Then, serialize.

    Then, push them back into the obj.

    """
    if not doc:
        return

    # pop the meta values (uid, created, updated, collection)
    meta_values = dict((k, doc.pop(k)) for k in META_KEYS if k in doc)

    # serialize ourself without the meta values
    serialized = serialize(doc)

    # restore the meta values
    doc.update(meta_values)

    return serialized


def _row_to_doc(cursor, transformer=None):
    """
    Unserialize from a db cursor of a row in the entity table to a doc
    
    """
    if not cursor:
        return

    uid = cursor["uid"]
    parent_uid = cursor["parent_uid"]
    collection = cursor["collection"]
    version = cursor["version"]
    created = cursor["created"]
    updated = cursor["updated"]
    payload = cursor["payload"]
    
    doc = unserialize(payload, cache_buster_id="{0}::{1}".format(uid, version))
    if not isinstance(doc, dict):
        doc = {META_KEY_PAYLOAD:doc}

    doc[META_KEY_UID] = uid
    doc[META_KEY_PARENT_UID] = parent_uid
    doc[META_KEY_COLLECTION] = collection
    doc[META_KEY_VERSION] = version
    doc[META_KEY_CREATED] = created 
    doc[META_KEY_UPDATED] = updated

    if transformer and callable(transformer):
        doc = transformer(doc)

    return doc


def _patch_doc(d, u):
    """
    recursively update a nested dictionary, similar to dict.update
    http://stackoverflow.com/questions/3232943/update-value-of-a-nested-dictionary-of-varying-depth

    >>> a = {"a":1, "b":{"c":123}}
    {'a':1, 'b':{'c':123}}
    >>> _patch_doc(a, {"a":{"e":987}})
    {'a': {'e': 987}, 'b': {'c': 123}}
    >>> _patch_doc(a, {"b":{"d":345}})
    {'a': {'e': 987}, 'b': {'c': 123, 'd': 345}}
    
    """
    iterable = u.iteritems() if hasattr(u, "iteritems") else u.items()
    for k, v in iterable:
        if isinstance(v, collections.Mapping):
            r = _patch_doc(d.get(k, {}), v)
            d[k] = r
        else:
            d[k] = u[k]
    return d


def _transform_doc_fields(fields=None):
    """
    Transform a doc to only return a subset of fields

    `fields` is an array of field mappings. either:
        1) a string of the key
        2) a tuple of two elements (key, mappedKey) or (key, convertFn)
        3) a tuple of three elements (key, mappedKey, convertFn)

    """
    if fields:
        if isinstance(fields, str):
            fields = (fields,)

        def _transformer(doc):
            if fields is None:
                return doc

            doc = Dot(doc)
            newDoc = Dot()

            for field in fields:
                convertFn = None
                if isinstance(field, tuple):
                    tupleLen = len(field)
                    if tupleLen == 1:
                        key = mappedKey = field[0]
                    elif tupleLen == 2:
                        key, mappedKey = field
                        # handle the case if tuple[1] is the convert function
                        if callable(mappedKey):
                            convertFn = mappedKey
                            mappedKey = key
                    elif tupleLen == 3:
                        key, mappedKey, convertFn = field
                    else:
                        raise ValueError("Field must be a string or a tuple "
                            "with 1 to 3 elements (key, mappedKey, convertFn)")
                else:
                    key = mappedKey = field

                if convertFn and callable(convertFn):
                    newDoc[key] = convertFn(doc.get(mappedKey, None))
                else:
                    newDoc[key] = doc.get(mappedKey, None)
            return newDoc.value
        return _transformer


def _index_key(collection, fields, sep=":", unique=False, sparse=False):
    """
    Generate an index table name from the parameters

    """
    key = ""
    
    if not isinstance(fields, list) and not isinstance(fields, tuple):
        fields = (fields,)

    for field in fields:
        key += "{0}{1}".format(sep, field)
    
    prefix = "index"
    
    if unique:
        prefix = "unique{0}".format(prefix)

    if sparse:
        prefix = "sparse{0}".format(prefix)
    
    return "{1}{0}{2}{3}".format(sep, prefix, collection, key)


"""
Generic Utilities

"""

Node = collections.namedtuple("Node", ["parent", "child"])


class Dot(object):
    """
    Container interface to a dictionary for using dot accessors
    
    """
    @staticmethod
    def dot(obj, key, value=None, op="get", sep="."):
        """
        Access a dictionary object using a dotted notation with sep. 
        
        Examples:
        >>> dot(dict({a:{b:1}}), "a.b", op="get") == 1
        True

        >>> dot(dict({a:{b:1}}), "a:b", sep=":") == 1
        True

        """
        if sep not in key:
            if op == "set":
                obj[key] = value
                return
            elif op == "get":
                return obj[key]
            elif op == "del":
                del obj[key]
                return
            raise ValueError("op parameter not valid")

        if not isinstance(obj, dict):
            raise ValueError("obj parameter must be of type `dict`")
        
        segs = key.split(sep)
        first, rest = segs[0], segs[1:] if len(segs) > 1 else []
        
        if first not in obj:
            if op == "get" or op == "del":
                raise KeyError("dict does not contain key {0}".format(first))
            if op == "set":
                obj[first] = {}
        
        remaining_obj = obj[first]
        remaining_key = sep.join(rest)

        return Dot.dot(remaining_obj, remaining_key, value=value, op=op,
            sep=sep)

    @staticmethod 
    def flatten_dict(d, parent_key="", sep="."):
        items = []
        for k, v in d.items():
            new_key = parent_key + sep + k if parent_key else k
            if isinstance(v, collections.MutableMapping):
                items.extend(Dot.flatten_dict(v, new_key).items())
            else:
                items.append((new_key, v))
        return dict(items)


    def __init__(self, value=None, sep="."):
        if value is None:
            value = {}
        
        if not isinstance(value, dict):
            raise ValueError("Value {0} must be a dictionary".format(value))

        self.value = value
        self.sep = sep

    
    def get(self, key, default_=None):
        try:
            return self.__getitem__(key)
        except KeyError:
            return default_


    def flatten(self, sep=None):
        if sep is None:
            sep = self.sep
        return Dot.flatten_dict(self.value, sep=sep)


    def __getitem__(self, key):
        return Dot.dot(self.value, key, op="get", sep=self.sep)
    

    def __contains__(self, key):
        try:
            value = Dot.dot(self.value, key, op="get", sep=self.sep)
            return True
        except KeyError:
            return False
    

    def __setitem__(self, key, value):
        return Dot.dot(self.value, key, value=value, op="set", sep=self.sep)
    

    def __delitem__(self, key):
        return Dot.dot(self.value, key, op="del", sep=self.sep)


    def __str__(self):
        if self.value:
            return self.value.__str__()


    def __repr__(self):
        if self.value:
            return self.value.__repr__()


def serialize(obj, use_pickle=False, use_bson=False, compress=False, 
        encoding="utf-8"):
    """
    Serialize a python dictionary object

    If `compress` is True, this function will use zlib to compress the
    serialized object.

    If `use_bson` is True, this function will try to serialize to bson
    instead of json.

    `encoding` is used when using zlib with json. json must be encoded
    into bytes before passing to zlib when `compress` is True.

    """
    if not obj:
        return None

    if PICKLE_AVAILABLE and use_pickle:
        obj_serialized = pickle.dumps(obj)
    elif BSON_AVAILABLE and use_bson:
        obj_serialized = bson.serialize_to_bytes(obj)
    else:
        obj_serialized = json.dumps(obj)

    if compress:
        if isinstance(obj_serialized, str):
            obj_serialized = obj_serialized.encode(encoding)
        return zlib.compress(obj_serialized)

    return obj_serialized


def _lru_cache(maxsize=100):
    """
    Least-recently-used cache decorator (to be used for Python < 3.2).

    Arguments to the cached function must be hashable.
    Cache performance statistics stored in f.hits and f.misses.
    Clear the cache with f.clear().
    http://en.wikipedia.org/wiki/Cache_algorithms#Least_Recently_Used
    http://code.activestate.com/recipes/498245-lru-and-lfu-cache-decorators/    

    """
    import collections
    maxqueue = maxsize * 10
    def decorating_function(user_function,
            len=len, iter=iter, tuple=tuple, sorted=sorted, KeyError=KeyError):
        cache = {}                  # mapping of args to results
        queue = collections.deque() # order that keys have been used
        refcount = collections.Counter() # times each key is in the queue
        sentinel = object()         # marker for looping around the queue
        kwd_mark = object()         # separate positional and keyword args

        # lookup optimizations (ugly but fast)
        queue_append, queue_popleft = queue.append, queue.popleft
        queue_appendleft, queue_pop = queue.appendleft, queue.pop

        @functools.wraps(user_function)
        def wrapper(*args, **kwds):
            # cache key records both positional and keyword args
            key = args
            if kwds:
                key += (kwd_mark,) + tuple(sorted(kwds.items()))

            # record recent use of this key
            queue_append(key)
            refcount[key] += 1

            # get cache entry or compute if not found
            try:
                result = cache[key]
                wrapper.hits += 1
            except KeyError:
                result = user_function(*args, **kwds)
                cache[key] = result
                wrapper.misses += 1

                # purge least recently used cache entry
                if len(cache) > maxsize:
                    key = queue_popleft()
                    refcount[key] -= 1
                    while refcount[key]:
                        key = queue_popleft()
                        refcount[key] -= 1
                    del cache[key], refcount[key]

            # periodically compact the queue by eliminating duplicate keys
            # while preserving order of most recent access
            if len(queue) > maxqueue:
                refcount.clear()
                queue_appendleft(sentinel)
                for key in itertools.ifilterfalse(refcount.__contains__,
                                        iter(queue_pop, sentinel)):
                    queue_appendleft(key)
                    refcount[key] = 1

            return result

        def clear():
            cache.clear()
            queue.clear()
            refcount.clear()
            wrapper.hits = wrapper.misses = 0

        wrapper.hits = wrapper.misses = 0
        wrapper.clear = clear
        return wrapper
    return decorating_function


# if Python >= 3.x, use the lru_cache provided by functools
if sys.version_info < (3,0):
    lru_cache = _lru_cache
else:
    lru_cache = functools.lru_cache


def ordered_json_loads(*args, **kwargs):
    """ load a json string into an OrderedDict """
    return json.loads(*args,
        object_pairs_hook=collections.OrderedDict, **kwargs)


@lru_cache(maxsize=10)
def unserialize(obj_serialized, cache_buster_id=None, use_pickle=False,
        use_bson=False, decompress=False, encoding="utf-8"):
    """
    Unserialize from a serialized object to python dict object

    If `decompress` is True, this function will use zlib to decompress
    the serialized object.

    If `use_bson` is True, this function will try to serialize to bson
    instead of json.
    
    `encoding` is used when using zlib with json when `decompress` is True.
    zlib returns bytes which need to be encoded into a str for json.dumps.

    """
    if not obj_serialized:
        return None
    
    if decompress:
        obj_serialized = zlib.decompress(obj_serialized)
    
    if PICKLE_AVAILABLE and use_pickle:
        return pickle_loads(obj_serialized)
    elif BSON_AVAILABLE and use_bson:
        return bson.parse_bytes(obj_serialized)
    
    if isinstance(obj_serialized, bytes):
        obj_serialized = obj_serialized.decode(encoding)

    return ordered_json_loads(obj_serialized)


def create_uid():
    """Generate and return a unique id for a document"""
    return adapt_uid(uuid.uuid4())
    #return uuid.uuid4()


def adapt_uid(uid):
    """Convert from UUID to string"""
    return uid.hex


def convert_uid(value):
    """Convert from string value to UUID object"""
    return value.decode("utf-8")
    #return uuid.UUID(hex=value.decode("utf-8"))


def payload_field(rowid, obj, path, default_=None):
    """
    A custom sqlite3 function for accessing the field data of an object
    that has been serialized using our serialize/unserialize functions

    """
    try:
        if not obj or not path:
            return default_
        return Dot(unserialize(obj, cache_buster_id=rowid)).get(path, default_)
    except Exception as e:
        return default_


@lru_cache(maxsize=10)
def _regex(expr):
    """
    Create a regex function and cache the 5 most recently used expressions

    """
    return re.compile(expr)


def regexp(expr, item):
    """
    A custom sqlite3 function for comparing fields via Regular Expressions

    """
    if item is None:
        return False
    return _regex(expr).search(item) is not None


def datetime_now():
    """
    Return a datetime of now without microseconds (for json compatibility).
    This returns the datetime.now in UTC

    """
    now = datetime.datetime.utcnow()
    now = now.replace(microsecond=0)
    return now


def escape(value):
    """
    Escape a SQLite table name, or column name
    
    """
    return "\"{0}\"".format(value.replace("'", "''"))



class groupby(dict):
    """
    SQL-like groupby statement. Returns a dict of groups.

    >>> a = [{"char":"a", "new":True}, {"char":"b", "new":True}, {"char":"c", "new":False}]
    >>> groupby(a, key=lambda x: x["new"])
    {False: [{'char': 'c', 'new': False}], True: [{'char': 'a', 'new': True}, {'char': 'b', 'new': True}]}

    """
    def __init__(self, seq, key=lambda x:x):
        for value in seq:
            k = key(value)
            self.setdefault(k, []).append(value)
    __iter__ = dict.iteritems if hasattr(dict, "iteritems") else dict.items


if __name__ == "__main__":
    print("please feel free to import this module. wf!")

