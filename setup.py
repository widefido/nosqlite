#!/usr/bin/env python

from distutils.core import setup

setup(name="nosqlite",
      description="NoSQLite is a \"nosql\" document-oriented database written in Python that leverages SQLite3 as its storage backend.",
      url="http://bitbucket.org/widefido/nosqlite",
      version="0.0",
      author="Jordan Sherer",
      author_email="jordan@widefido.com",
      py_modules=["nosqlite"],
      package_dir={"":"src"}
)
